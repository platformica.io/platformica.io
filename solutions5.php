<?php

/**
 * Template Name: Platformica #solutions5
 *
 * @package platformica
 */

?>
<?php get_header(); ?>


<?php
    $areas = [];
    $areas[] = (object) [
        'src' => 'images/svg/baremetal.svg',
        'title' => 'Compute',
        'href' => '/solution1/',
        'text' => ''
    ];

    $areas[] = (object) [
        'src' => 'images/svg/baremetal.svg',
        'title' => 'Networking',
        'href' => '/solution2/',
        'text' => ''
    ];

    $areas[] = (object) [
        'src' => 'images/svg/baremetal.svg',
        'title' => 'Storage',
        'href' => '/solution3/',
        'text' => ''
    ];

?>

<svg style="max-width:1500px" data-square='{ "image" : "https://www.platformica.io/wp-content/themes/platformica/images/png/d.png" , "threshold" : 0.1 , "pixel" : 2 , "gutter" : 2 , "width" : 1500 }' ></svg>
<section id="solution">
    <div class="grid-container">
        <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12">
                <div class="component heading">
                    <h1>Bare metal cloud</h1>
                    <h4></h4>
                    <hr />
                </div>
            </div>
        </div>
    </div>
</section>
<section id="solution-5-1" class="general">
    <div class="grid-container">
        <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12">
                <div class="component text styled">
                    <p>Physical server, network and storage infrastructure are foundation for any application platform whether it is a low level layer for private cloud based on OpenStack, VMware, Kubernetes or plain single purpose bare metal infrastructure for specific applications. Many operators realize that provisioning, management and whole physical infrastructure lifecycle can become a bottleneck in any project which tries to improve reliability and flexibility of providing resources to applications.</p>
                    <p>Platformica builds fully automated vendor-agnostic bare metal infrastructures where control over compute, storage and networking resources is fully at disposal of datacenter sysadmins using software defined methods.</p>
                </div>
            </div>
        </div>
        <hr style="margin-top: 48px; margin-bottom: 48px; width: 10%;" />
        <h4 style="text-align: center; ">Our Bare metal solutions cover all 3 main infrastructure areas</h4>
        <div class="grid-x grid-margin-x grid-margin-y small-up-2 medium-up-5 large-up-5">

            <div class="cell hide-for-small-only"></div>
            <?php foreach($areas as $area): ?>
            <div class="cell">
                <div class="component solution-1" >
                    <div class="grid-x">
                        <div class="cell small-12">
                            <div class="overlay" style="background: url('<?php echo get_template_directory_uri(); ?>/<?php echo $area->src; ?>') center center/cover;">
                            </div>
                        </div>
                        <div class="cell small-12">
                            <div class="content">
                                <h5><strong><?php echo $area->title; ?></strong></h5>
                                <p><?php echo $area->text; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<section id="solution-5-2">
    <div class="grid-container">
        <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12 medium-6 small-order-2 medium-order-1">
                <div class="component splashimage">
                    <div class="grid-x">
                        <div class="cell shrink">
                            <?php include("images/svg/baremetal.svg"); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cell small-12 medium-6 small-order-1 medium-order-2">
                <div class="component text styled">
                    <h4>Compute</h4>
                    <p>Automation of compute includes all aspects of server configuration up to installing Operating systems and deploying initial operating system configs. We also take care of automated BIOS configuration and make them consistent across datacenter, we automate and standardize scaling of physical server infrastructure and last but not least we automate management, creation and distribution of Operating system images.</p>
                </div>
            </div>
        </div>
        <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12 medium-6">
                <div class="component text styled">
                    <h4>Networking</h4>
                    <p>Automation for networking resources includes configuration and lifecycle for datacenter networking devices such as ToR, Aggregation and Core switches, routers and firewalls. We can turn manually managed VLANs, VXLANs, VPNs and IP ranges into well managed pools of dynamically allocated resources that are consistently configured across whole networking infrastructure. Changes on this network layer can be seamlessly propagated to upper software layers (for example into the cloud platform like OpenStack) - all this without complex and centralized SDN controllers. We can help customers to automate management of EVPN fabrics which can then serve as a universal communication domain across multiple application environments.</p>
                </div>
            </div>
            <div class="cell small-12 medium-6">
                <div class="component splashimage">
                    <div class="grid-x">
                        <div class="cell shrink">
                            <?php include("images/svg/baremetal.svg"); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12 medium-6 small-order-2 medium-order-1">
                <div class="component splashimage">
                    <div class="grid-x">
                        <div class="cell shrink">
                            <?php include("images/svg/baremetal.svg"); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cell small-12 medium-6 small-order-1 medium-order-2">
                <div class="component text styled">
                    <h4>Storage</h4>
                    <p>For customers who want to build storage layer using off the shelf servers and SDS software like CEPH we automate complete setup of storage clusters and their network integration into the compute clusters. Thanks to this automation it is then easy to treat whole storage as a multi-tenant resource where it is possible to instantiate multiple independent storage clusters which only share administration/automation plane but from the workload perspective they are completely independent. This is often crucial because storage layer is especially sensitive to noisy neighbor problem and customers often want to separate data for various application on the very hardware layer.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer();

