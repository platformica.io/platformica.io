<?php

/**
 * Template Name: Platformica #careers
 *
 * @package platformica
 */

?>
<?php get_header(); ?>


<svg style="max-width:1500px" data-square='{ "image" : "https://www.platformica.io/wp-content/themes/platformica/images/png/d.png" , "threshold" : 0.1 , "pixel" : 2 , "gutter" : 2 , "width" : 1500 }' ></svg>
<section id="careers-1">
    <div class="grid-container">
        <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12">
                <div class="component heading">
                    <h1>Careers</h1>
                    <h4>Are you a future Packeteer?</h4>
                    <hr />
                </div>
            </div>
        </div>
    </div>
</section>
<section style="padding: 96px 0" class="general noshadow">
    <div class="grid-container">
        <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12 medium-6 small-order-2 medium-order-1">
                <div class="component text styled">
                    <h4>Are you a future Packeteer?</h4>
                    <p>Packet is a growing team of software engineers, network builders, creative designers, hardware nerds and customer success gurus dedicated to helping companies turn infrastructure into a competitive advantage.</p>
                    <a class="button"  href="#">View Open Positions</a>
                </div>
            </div>
            <div class="cell small-12 medium-6 small-order-1 medium-order-2">
                <div class="component splashimage">
                    <div class="grid-x">
                        <div class="cell shrink" >
                            <img id="test2" src="<?php echo get_template_directory_uri(); ?>/images/jpg/careers.jpg" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="test" style="background-image: linear-gradient(-180deg,rgba(54,66,74,.6),rgba(54,66,74,.6) 85%), url('<?php echo get_template_directory_uri(); ?>/images/jpg/back-service-text_thumb-desktop_wide.jpg');">
    <div class="grid-container">
        <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12 medium-12 small-order-2 medium-order-1">
                <div class="component text styled">
                    <h4>Work with us, no matter where you are.</h4>
                    <p>Since its founding in 2014 Packet has evolved from a NYC-centric company to a passionate family living and working in 10 countries. We're committed to building a transparent, values-driven culture that helps us succeed while making a positive difference in our communities.</p>
                    <p>We are a remote-friendly company that comes together to solve hard problems and make a difference. If this resonates with you, we should talk!</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="grid-container">
        <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-6 medium-6 small-order-2 medium-order-1">
                <div class="component text styled">
                    <h4>Perks & Benefits</h4>
                </div>
            </div>
            <div class="cell small-6 medium-6 small-order-2 medium-order-1">
                <div class="component text styled">
                    <ul>
                        <li><p><strong style="font-weight: 900;">Insurance:</strong> 100% coverage for the standard health, vision & dental insurance plans for you and your family.</p></li>
                        <li><p><strong style="font-weight: 900;">Retirement:</strong> 401(k) with up to 6% employer match, capped at $3k.</p></li>
                        <li><p><strong style="font-weight: 900;">Stock Options:</strong> employees are eligible for participation in our stock option program based on performance.</p></li>
                        <li><p><strong style="font-weight: 900;">Time Off:</strong> packet provides a flexible vacation policy built on employee trust.</p></li>
                        <li><p><strong style="font-weight: 900;">Great Tools:</strong> employees have a choice of computing hardware paid for by Packet and a stipend for your cell phone.</p></li>
                        <li><p><strong style="font-weight: 900;">Transparency:</strong> everyone has access to business metrics and financial information about the company.</p></li>
                    </ul>
                </div>
            </div>
            <div class="cell small-12 small-order-2 medium-order-1">
                <div class="component splashimage">
                    <div class="grid-x">
                        <div class="cell shrink" >
                            <img id="test2" src="<?php echo get_template_directory_uri(); ?>/images/jpg/c0.jpg" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="grid-container">
        <div class="grid-x">
            <div class="cell small-offset-2 small-8">

                <div class="grid-x grid-margin-x grid-margin-y small-up-1 medium-up-2">
                    <?php foreach(getData()['jobs'] as $job): ?>
                        <div class="cell">
                            <div class="component job" >
                                <h6><?php echo $job->title; ?></h6>
                                <p><?php echo $job->text; ?></p>
                                <?php include('images/svg/next.svg'); ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <hr style="margin: 96px auto;" />
</section>
<?php get_footer();

