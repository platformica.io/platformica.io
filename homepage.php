<?php

/**
 * Template Name: Platformica #homepage
 *
 * @package platformica
 */

?>
<?php get_header(); ?>
<?php

    $testimonials = [];
    $pillars = [];
    $whitepapers = [];
    $technologies = [];

    $customers_long = [];


    $testimonials[] = (object) [
        'title' => 'Disaggregation approach',
        'text' => 'Our disaggregation approach stands on three key areas which together form complete end-to-end infrastructure strategy. The three key areas are - ability to build various infrastructures in their primary deployment schemes, ability to use various platform components (firewalls, load balancers, databases) and ability to automate heterogenous applications environments.</p><p style="text-align: center;"><a class="button" style="border-radius:8px;color: #161818;width: 140px; font-size: 16px; font-weight: bold; margin:0px;background: #f0f4f7; border: 0px; padding: 4px 8px;" href="/company/#our-vision">Our vision</a>'
    ];
    $testimonials[] = (object) [
        'title' => 'Infrastructure with focus on simplicity',
        'text' => 'We are building the infrastructure layer simple and shift complexity to upper layers where more complex resources can be accuratly targeted to workloads which need them. This allows treating infrastructure layer as a commodity resource trus reducing technological lockin and increasing feature flexiblity on upper layers.'
    ];
    $testimonials[] = (object) [
        'title' => 'We build production-grade components',
        'text' => 'Platformica builds and supports platform level appliances (and appliance clusters) which thoroughly encapsulate various applications and software components into well defined objects which can be deployed, reused and scaled. These appliances can serve as alternative for vertically integrated services in public/private cloud platforms.'
    ];
    $testimonials[] = (object) [
        'title' => 'Automation brings everything together',
        'text' => 'Automation brings together basic IaaS resources and disaggregated platform components and enables building complex applications. Automation and Infrastructure as code approach is lightweight and easier to fulfill specific business needs compared to out of the box vertically integrated heavyweight platforms.'
    ];
    $testimonials[] = (object) [
        'title' => 'Taking advantage of Multi-Cloud',
        'text' => 'Platformica helps organization to embrace multi-cloud strategies not only in public cloud world but also in private datacenters. Check out our heterogenous datacenter network presentation to see very real and specific details on how to get closer to multi cloud vision in your datacenter and no longer be locked in one-size fits all application environment.'
    ];


    $pillars[] = (object) [
        'svg' => 'images/svg/infrastructure.svg',
        'title' => 'Infrastructure',
        'text' => 'Public cloud, Private cloud,<br />Datacenter, Networks'
    ];


    $pillars[] = (object) [
        'svg' => 'images/svg/components.svg',
        'title' => 'Platform components',
        'text' => 'Databases, Data frameworks,<br />Load balancers, Firewalls'
    ];

    $pillars[] = (object) [
        'svg' => 'images/svg/automation.svg',
        'title' => 'Automation',
        'text' => 'Infrastructure as Code,<br />DevOps, CD/CI'
    ];

    $whitepapers[] = (object) [
        'title' => 'Operator series: Building Ideal Edge Cloud Infrastructure',
        'href' => '#1'
    ];

    $whitepapers[] = (object) [
        'title' => 'Public cloud independence: Platformica Appliances',
        'href' => '#2'
    ];

    $whitepapers[] = (object) [
        'title' => 'Operator series: Building Ideal Edge Cloud Infrastructure',
        'href' => '#3'
    ];

    $whitepapers[] = (object) [
        'title' => 'Operator series: Building Ideal Edge Cloud Infrastructure',
        'href' => '#4'
    ];

    $whitepapers[] = (object) [
        'title' => 'Public cloud independence: Platformica Appliances',
        'href' => '#5'
    ];

    $whitepapers[] = (object) [
        'title' => 'Operator series: Building Ideal Edge Cloud Infrastructure',
        'href' => '#6'
    ];

    $technologies[] = (object) [
        'src' => 'images/png/300x120/kubernetes.png'
    ];
    $technologies[] = (object) [
        'src' => 'images/png/300x120/openstack.png'
    ];
    $technologies[] = (object) [
        'src' => 'images/png/300x120/linux.png'
    ];
    $technologies[] = (object) [
        'src' => 'images/png/300x120/traefik.png'
    ];
    $technologies[] = (object) [
        'src' => 'images/png/300x120/haproxy.png'
    ];
    $technologies[] = (object) [
        'src' => 'images/png/300x120/mariadb.png'
    ];
    $technologies[] = (object) [
        'src' => 'images/png/300x120/frrouting.png'
    ];
    $technologies[] = (object) [
        'src' => 'images/png/300x120/prometheus.png'
    ];

    $customers_long[] = (object) [
        'src' => 'images/png/300x167/cra.png',
        'title' => 'České Radiokomunikace',
        'text' => '<p>Deploying and supporting openstack based infrastructure for providing media processing and telco services. Creating integrated multicloud solution compatible with VMWare. Deploying and supporting openstack based infrastructure for providing media processing and telco services. Creating integrated multicloud solution compatible with VMWare.</p>'
    ];
    $customers_long[] = (object) [
        'src' => 'images/png/300x167/pixel.png',
        'title' => 'Pixel Federation',
        'text' => '<p>Deploying mobile applications to public cloud and providing continuous integration and continuous delivery automation using modern tools like Ansible. Deploying and supporting kubernetes based infrastructure running in public cloud. Deploying mobile applications to public cloud and providing continuous integration and continuous delivery automation using modern tools like Ansible. Deploying and supporting kubernetes based infrastructure running in public cloud.</p>'
    ];
    $customers_long[] = (object) [
        'src' => 'images/png/300x167/ibuldings.png',
        'title' => 'ibuildings',
        'text' => '<p>Deploying mobile applications to public cloud and providing continuous integration and continuous delivery automation using modern tools like Ansible. Deploying and supporting kubernetes based infrastructure running in public cloud. Deploying mobile applications to public cloud and providing continuous integration and continuous delivery automation using modern tools like Ansible. Deploying and supporting kubernetes based infrastructure running in public cloud.</p>'
    ];


?>

<svg id="homepage" data-square='{ "data" : "https://www.platformica.io/wp-content/themes/platformica/json/110m.json" , "fill" : "rgba(240, 244, 247, 0.15)" ,"threshold" : 0, "pixel" : 4 , "gutter" : 4 }'></svg>
<section id="splashscreen" >
    <div class="grid-container" style="display: flex; flex-flow: column;">
        <div class="grid-x small-order-2 medium-order-1">
            <div class="cell small-offset-2 small-8 medium-offset-0 medium-4" style="position: relative;">
                <svg data-testimonials-next style="display: block; margin: auto;" data-circle='{"start":-90,"type":"master","size":"outerWidth","media":"<?php echo get_template_directory_uri(); ?>/images/jpg/svg.jpg"}'></svg>
            </div>
            <div class="cell small-12 show-for-small-only" style="align-self: center; text-align: center;">
                <h4  data-testimonials-title style="font-size: 22px; line-height: 24px; "><?php echo $testimonials[0]->title; ?></h4>
            </div>
            <div class="cell small-12 medium-8" >
                <div class=" swiper-pagination"></div>
                <div class="component swiper-container testimonials" data-testimonials="{}">
                    <div class="swiper-wrapper">
                        <?php foreach ($testimonials as $testimonial): ?>
                        <div class="swiper-slide">
                            <div data-title="<?php echo $testimonial->title; ?>" class="component testimonial" style="align-items: center;">
                                <div class="component text">
                                    <h4 class="hide-for-small-only"><?php echo $testimonial->title; ?></h4>
                                    <p style="text-align: justify;"><?php echo $testimonial->text; ?></p>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="grid-x small-order-1 medium-order-2 pillars">
            <?php foreach ($pillars as $i => $pillar): ?>
            <div class="cell small-4 medium-4">
                <div class="component pillar" data-testimonials-click='{"slide": <?php echo $i+2; ?>}'>
                    <div class="grid-x">
                        <div class="cell small-12 medium-4">
                            <svg data-circle='{"start":-90,"type":"slave", "size":"outerHeight"}'></svg>
                            <?php include($pillar->svg); ?>
                        </div>
                        <div class="cell small-12 medium-8">
                            <h5><?php echo $pillar->title; ?></h5>
                            <p><?php echo $pillar->text; ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<section id="whitepapers" style="background: url('<?php echo get_template_directory_uri(); ?>/images/jpg/whitepapers.jpg') center bottom/cover;">
    <div class="grid-container">
        <div class="grid-x">
            <div class="cell small-12">
                <div class="component heading">
                    <h1>Resources</h1>
                </div>
            </div>
        </div>
        <div class="grid-x" style="align-items: center;">
            <div class="cell small-1">
                <div class="swiper-button-prev"><?php include('images/svg/prev.svg'); ?></div>
            </div>
            <div class="cell small-10" style="position: relative;">
                <div class="component swiper-container whitepapers" data-whitepapers="{}">
                    <div class="swiper-wrapper">
                        <?php foreach($whitepapers as $whitepaper): ?>
                        <div class="swiper-slide">
                            <div class="component whitepaper styled">
                                <div class="grid-x">
                                    <div class="cell small-4">
                                        <?php include('images/svg/whitepaper.svg'); ?>
                                    </div>
                                    <div class="cell small-8">
                                        <p><?php echo $whitepaper->title; ?></p>
                                        <a href="<?php echo $whitepaper->href; ?>" >Download now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="cell small-1">
                <div class="swiper-button-next" ><?php include('images/svg/next.svg'); ?></div>
            </div>
        </div>
    </div>
</section>
<section id="technologies">
    <div class="grid-container">
        <div class="grid-x">
            <div class="cell small-12">
                <div class="component heading">
                    <h1>Technologies</h1>
                    <h4>We use modern open source software</h4>
                    <hr />
                </div>
            </div>
        </div>
        <div class="grid-x">
            <div class="cell small-12">
                <div class="component cloud">
                    <!-- ??? //-->
                    <ul class="grid-x grid-margin-x grid-margin-y">
                        <?php foreach ($technologies as $technology): ?>
                        <li class="cell small-6 medium-3 ">
                            <div>
                                <img src="<?php echo get_template_directory_uri(); ?>/<?php echo $technology->src; ?>" />
                            </div>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                    <!-- ??? //-->
                </div>
            </div>
        </div>
        <!--<div class="grid-x">
            <div class="cell small-12">
                <div class="component button">
                    <a class="button" href="#">Learn more</a>
                </div>
            </div>
        </div> -->
    </div>
</section>
<section id="customers">
    <div class="grid-container fluid">
        <div class="grid-x">
            <div class="cell small-12">
                <div class="component heading">
                    <h1>Customers</h1>
                    <h4>Who did choose Platformica</h4>
                    <hr />
                </div>
            </div>
        </div>
        <div class="grid-x grid-margin-x grid-margin-y customers">
            <?php foreach($customers_long as $customer): ?>
            <div class="cell small-12 medium-4">
                <div class="component customer">
                    <img src="<?php echo get_template_directory_uri(); ?>/<?php echo $customer->src;?>" />
                    <h4><?php echo $customer->title; ?></h4>
                    <?php echo $customer->text; ?>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
        <div class="grid-x">
            <div class="cell small-12">
                <div class="component button">
                    <a class="button" href="/company/">Learn more</a>
                </div>
            </div>
        </div>
    </div>
</section>




<?php get_footer();
