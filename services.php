<?php

/**
 * Template Name: Platformica #services
 *
 * @package platformica
 */

?>
<?php get_header(); ?>

<?php



?>
<svg style="max-width:1500px" data-square='{ "image" : "https://www.platformica.io/wp-content/themes/platformica/images/png/d.png" , "threshold" : 0.1 , "pixel" : 2 , "gutter" : 2 , "width" : 1500 }' ></svg>
<section>
    <div class="grid-container">
        <div class="grid-x">
            <div class="cell small-12">
                <div class="component heading">
                    <h1>Our Services</h1>
                    <h4>Application infrastructure solutions for enterprises and operators</h4>
                    <hr />
                </div>
            </div>
        </div>
    </div>
</section>
<section id="services-list">
    <div class="grid-container">


        <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12">
                <div class="component text styled">
                    <p style="font-size: 23px;line-height: 31px;">Our mission is to help organizations change their operating model, build own, internal knowledge and reduce dependencies on legacy suppliers and infrastructure. Platformica provides services in 4 categories.</p>
                </div>
            </div>
        </div>

        <div class="grid-x grid-margin-x grid-margin-y small-up-1 medium-up-4" style="justify-content: center;">
            <?php foreach(getData()['services'] as $service): ?>
            <div class="cell">
                <div class="component service">
                    <div class="grid-x">
                        <div class="cell small-12">
                            <div style="max-width: 225px; margin: auto;">
                                <div class="overlay" style="background: url('<?php echo get_template_directory_uri(); ?>/<?php echo $service->src; ?>') center center/cover;">
                                </div>
                            </div>
                            <h5 style="font-weight: bold;"><?php echo $service->title; ?></h5>
                            <p><?php echo $service->text; ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>



<section class="test" style="background-image: linear-gradient(-180deg,rgba(54,66,74,.6),rgba(54,66,74,.6) 85%), url('<?php echo get_template_directory_uri(); ?>/images/jpg/back-service-text_thumb-desktop_wide.jpg');">
    <div class="grid-container">
        <div class="grid-x">
            <div class="cell auto">
                <div class="component text styled blockquote">
                    <blockquote style="color:white;">
                        We bring a disruptive approach to enterprises to embrace lock-free Multi-Cloud way of building application infrastructure.
                    </blockquote>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="general">
    <div class="grid-container">
        <div class="grid-x" style="align-items: center;">
            <div class="cell small-12 medium-7">
                <div class="component text styled blockquote" >
                    <blockquote style="color:black;">Get in touch to find out how we could help transforming your organization!</blockquote>
                </div>
            </div>
            <div class="cell small-12 medium-5">
                <div class="component text styled" style="text-align:center;">
                    <a style="background: #6bcb92;" class="button" href="/technical-support/">Get in touch with us</a>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer();

