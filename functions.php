<?php

####################### REMOVE #######################
add_post_type_support( 'page', 'excerpt' );// See http://wordpress.mfields.org/2010/excerpts-for-pages-in-wordpress-3-0/
remove_action('wp_head', 'rsd_link');// Windows Live Writer? Ew!
remove_action('wp_head', 'wlwmanifest_link');// Windows Live Writer? Ew!
remove_action('wp_head', 'wp_generator');// No need to know my WP version quite that easily
// remove WP 4.2+ emoji nonsense
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );
// remove WP 4.9+ dns-prefetch nonsense
remove_action( 'wp_head', 'wp_resource_hints', 2 );
// disable embeds nonsense; not even sure what it does
// Remove the REST API endpoint.
remove_action('rest_api_init', 'wp_oembed_register_route');
// Turn off oEmbed auto discovery.
// Don't filter oEmbed results.
remove_filter('oembed_dataparse', 'wp_filter_oembed_result', 10);
// Remove oEmbed discovery links.
remove_action('wp_head', 'wp_oembed_add_discovery_links');
// Remove oEmbed-specific JavaScript from the front-end and back-end.
remove_action('wp_head', 'wp_oembed_add_host_js');
// remove OneAll Social script from regular page
remove_action ('wp_head', 'oa_social_login_add_javascripts');
// Disable REST API link tag
remove_action('wp_head', 'rest_output_link_wp_head', 10);

// Disable oEmbed Discovery Links
remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);

// Disable REST API link in HTTP headers
remove_action('template_redirect', 'rest_output_link_header', 11, 0);


#######################

if( !is_admin() ) {
    //wp_enqueue_style( 'googlefont', 'https://fonts.googleapis.com/css?family=Barlow:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i');
    wp_enqueue_style( 'googlefont', 'https://fonts.googleapis.com/css?family=Barlow:100,200,300');
    wp_enqueue_style('paltformica', get_template_directory_uri() . '/assets/platformica.min.css',[],time());

    wp_enqueue_script('vendor',get_template_directory_uri().'/assets/vendor.min.js',[],false,true);
    wp_enqueue_script('paltformica', get_template_directory_uri() . '/assets/platformica.js',[], time(), true);
}

register_nav_menus(array(
    'primary' => esc_html__('Top primary menu', 'platformica')
));




require(__DIR__.'/data.php');
