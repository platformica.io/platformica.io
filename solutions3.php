<?php

/**
 * Template Name: Platformica #solutions3
 *
 * @package platformica
 */

?>
<?php get_header(); ?>
<?php
    $areas = [];
    $platforms = [];
    $appliances = [];
    $containers = [];
    $infrastructures = [];

    $areas[] = (object) [
        'src' => 'images/png/backup/underlying-infrastructure.png',
        'title' => 'Underlying infrastructure',
        'href' => '/solution1/',
        'text' => 'We build and support underlying infrastructure for Kubernetes (both private and public)'
    ];

    $areas[] = (object) [
        'src' => 'images/png/backup/containers.png',
        'title' => 'Kubernetes',
        'href' => '/solution2/',
        'text' => 'We build and support Kubernetes infrastructure'
    ];

    $areas[] = (object) [
        'src' => 'images/png/backup/application-layer-automation.png',
        'title' => 'Application layer automation',
        'href' => '/solution3/',
        'text' => 'We help with containerized application automation, delivery and integration'
    ];

    $areas[] = (object) [
        'src' => 'images/png/backup/platform-components.png',
        'title' => 'Platform components',
        'href' => '/solution4/',
        'text' => 'We build and support customized appliances and images built from open source software'
    ];

    $areas[] = (object) [
        'src' => 'images/png/backup/external-networks.png',
        'title' => 'External networks',
        'href' => '/solution4/',
        'text' => 'We help with networking integration to rest of customers infrastructure (both private and public)'
    ];

    $platforms[] = (object) [
        'src' => 'images/png/160x/openstack.png',
        'title' => 'OpenStack<br />Core projects'
    ];
    $platforms[] = (object) [
        'src' => 'images/png/160x/aws.png',
        'title' => 'Amazon AWS'
    ];
    $platforms[] = (object) [
        'src' => 'images/png/160x/vmware.png',
        'title' => 'VMware vSphere'
    ];
    $platforms[] = (object) [
        'src' => 'images/png/160x/google.png',
        'title' => 'Google App<br />Engine'
    ];
    $platforms[] = (object) [
        'src' => 'images/png/160x/baremetal.png',
        'title' => 'Bare Metal'
    ];



    $containers[] = (object) [
        'src' => 'images/png/300x120/kubernetes.png'
    ];
    $containers[] = (object) [
        'src' => 'images/png/300x120/flannel.png'
    ];
    $containers[] = (object) [
        'src' => 'images/png/300x120/rkt.png'
    ];
    $containers[] = (object) [
        'src' => 'images/png/300x120/docker.png'
    ];
    $containers[] = (object) [
        'src' => 'images/png/300x120/lstio.png'
    ];
    $containers[] = (object) [
        'src' => 'images/png/300x120/prometheus.png'
    ];
    $containers[] = (object) [
        'src' => 'images/png/300x120/projectcalico.png'
    ];
    $containers[] = (object) [
        'src' => 'images/png/300x120/cni.png'
    ];

    $infrastructures[] = (object) [
        'src' => 'images/png/160x/packer.png',
        'title' => 'Packer'
    ];
    $infrastructures[] = (object) [
        'src' => 'images/png/160x/saltstack.png',
        'title' => 'Saltstack'
    ];
    $infrastructures[] = (object) [
        'src' => 'images/png/160x/terraform.png',
        'title' => 'Terraform'
    ];
    $infrastructures[] = (object) [
        'src' => 'images/png/160x/ansible.png',
        'title' => 'Ansible'
    ];
?>


<svg style="max-width:1500px" data-square='{ "image" : "https://www.platformica.io/wp-content/themes/platformica/images/png/d.png" , "threshold" : 0.1 , "pixel" : 2 , "gutter" : 2 , "width" : 1500 }' ></svg>
<section id="solution-3">
    <div class="grid-container">
        <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12">
                <div class="component heading">
                    <h1>Integrated Kubernetes infrastructure</h1>
                    <h4>Kubernetes solution integrating with components hard to containerize</h4>
                    <hr />
                </div>
            </div>
        </div>
    </div>
</section>
<section class="general">
    <div class="grid-container">
        <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12">
                <div class="component text styled" >
                    <p>Kubernetes deliver new and attractive application platform platform. Not all application components are however good candidates to be containerized and the overall application design and achitecture should cope with parts of the applications exist outside of Kubernetes platform.</p>
                    <p>We realize this simple fact and build our minimalistic Kubernetes solutions to be optimal for running workload that is 100% "container-compatible" and keep rest of the workload outside of Kubernetes. We then use networking and automation layer integrations to provide full blown application environment for 100% of application components.</p>
                </div>
            </div>

        </div>
        <hr style="margin-top: 48px; margin-bottom: 48px; width: 10%;" />
        <h4 style="text-align: center; ">Our Kubernetes solutions stand on following 5 key parts</h4>
        <div class="grid-x grid-margin-x grid-margin-y small-up-1 medium-up-5" style="justify-content: center;">






            <?php foreach($areas as $area): ?>
            <div class="cell">
                <div class="component service">
                    <div class="grid-x">
                        <div class="cell small-12">
                            <div style="max-width: 225px; margin: auto;">
                                <!-- style="background: #454545;" -->
                                <div class="overlay"  style=" background: url('<?php echo get_template_directory_uri(); ?>/<?php echo $area->src; ?>') center center/contain transparent no-repeat;" >
                                    <!--<svg  style="z-index: 2;margin:0px;padding:0px;width: 193px;height: 193px;border-radius:99px;" data-square='{ "image" : "<?php echo get_template_directory_uri(); ?>/<?php echo $area->src; ?>" , "threshold" : 0.1 , "pixel" : 3 , "gutter" : 1 , "width" : 193 }' ></svg>-->
                                </div>
                            </div>
                            <h5 style="font-weight: bold;"><?php echo $area->title; ?></h5>
                            <p><?php echo $area->text; ?></p>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>
<section>
    <div class="grid-container">
        <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12 medium-6">
                <div class="component text styled">
                    <h4>Underlying infrastructure</h4>
                        <p>When building underlying infrastructure we follow minimalist approach and use minimal feature set on the IaaS level and leave complexity to upper layers. This has two effects. Firstly it minimizes infrastructure platform vendor lock in which results in better workload mobility, easier infrastructure evolution and easier cost optimizations. Secondly it maximizes flexibility of the upper layers which results in better fulfilling of specific business and operational needs. We build underlying infrastructure on following public and private cloud platforms:</p>
                </div>
            </div>
            <div class="cell small-12 medium-6">
                <div class="component splashimage">
                    <div class="grid-x">
                        <div class="cell shrink">
                            <div class="component solution-2" style="width: 300px;">
                                <!--<div class="overlay" style="background: url('<?php echo get_template_directory_uri(); ?>/images/png/t1.png') center center/cover;"></div>-->
                                <img src="<?php echo get_template_directory_uri(); ?>/images/png/t1b.png" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12 medium-6 small-order-2 medium-order-1">
                <div class="component splashimage">
                    <div class="grid-x">
                        <div class="cell small-10">
                            <div class="component solution-3">
                                <!--<div class="overlay" style="background: url('<?php echo get_template_directory_uri(); ?>/images/png/t4.png') center center/cover;"></div>-->
                                <img src="<?php echo get_template_directory_uri(); ?>/images/png/t4b.png" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cell small-12 medium-6 small-order-1 medium-order-2">
                <div class="component text styled">
                    <h4>Kubernetes</h4>
                    <p>We build and support Kubernetes infrastructure using variable stack of popular open source projects to meet specific customer needs. We build and support Kubernetes infrastructure using variable stack of popular open source projects to meet specific customer needs.</p>
                </div>
            </div>
        </div>
        <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12 medium-6">
                <div class="component text styled">
                    <h4>Application layer automation</h4>
                    <p>We help customers to automate complete application stack deployment. For more complex applications this often include components outside of container platform too. This is especially relevant for customers who for various reasons opt for Infra-as-code approach rather then using out-of-the-box PaaS solutions. We help customers with setting up frameworks for continuous delivery and continuous integration and help them to automate application delivery and integration within these frameworks.</p>
                </div>
            </div>
            <div class="cell small-12 medium-6">
                <div class="component splashimage">
                    <div class="grid-x">
                        <div class="cell shrink">
                            <div class="component solution-2" style="width: 300px;">
                                <!--<div class="overlay" style="background: url('<?php echo get_template_directory_uri(); ?>/images/png/t3.png') center center/cover;"></div>-->
                                <img src="<?php echo get_template_directory_uri(); ?>/images/png/t3b.png" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12 medium-6 small-order-2 medium-order-1">
                <div class="component splashimage">
                    <div class="grid-x">
                        <div class="cell small-8">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/png/t7.png" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="cell small-12 medium-6 small-order-1 medium-order-2">
                <div class="component text styled">
                    <h4>Platform components</h4>
                    <p>Platformica builds and supports ready to use appliances consisting of well integrated and packaged open-source software that are deployed in containers, virtual machines or bare metal servers based on specific customer requirements.</p>
                </div>
            </div>
        </div>
        <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12 medium-6">
                <div class="component text styled">
                    <h4>External networks</h4>
                    <p>We help with networking integration of Kubernetes platform to the rest of customers infrastructure (both private and public). Proper networking integration is crucial to successfully complete Kubernetes production deployment. More complex networking integrations and automations enable many advanced scenarios.</p>
                </div>
            </div>
            <div class="cell small-12 medium-6">
                <div class="component splashimage">
                    <div class="grid-x">
                        <div class="cell small-12">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/png/t5.png" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer();

