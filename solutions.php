<?php

/**
 * Template Name: Platformica #solutions
 *
 * @package platformica
 */

?>
<?php get_header(); ?>

<svg style="max-width:1500px" data-square='{ "image" : "https://www.platformica.io/wp-content/themes/platformica/images/png/d.png" , "threshold" : 0.1 , "pixel" : 2 , "gutter" : 2 , "width" : 1500 }' ></svg>
<section id="solutions">
    <div class="grid-container">
        <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12">
                <div class="component heading">
                    <h1>Disaggregated solutions</h1>
                    <h4>Application infrastructure solutions for enterprises and operators</h4>
                    <hr />
                </div>
            </div>
        </div>
    </div>
</section>
<section id="solutions-list">
    <div class="grid-container" >
        <div class="grid-x grid-margin-x  small-up-1 medium-up-2 large-up-3">
            <?php foreach(getData()['solutions'] as $solution): ?>
            <div class="cell">
                <div class="component solution">
                    <div class="grid-x">
                        <div class="cell small-12 medium-7" >
                            <div class="overlay" style="background: url('<?php echo get_template_directory_uri(); ?>/<?php echo $solution->src; ?>') center center/cover;">
                                <div class="content">
                                    <h5><?php echo $solution->title; ?></h5>
                                    <a class="button" href="<?php echo $solution->href; ?>">Read More</a>
                                </div>
                            </div>
                        </div>
                        <div class="cell small-12 medium-5">
                            <div class="component text">
                                <p><?php echo $solution->text; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cell show-for-small-only">
                <hr style="margin: 48px auto;" />
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>


<?php get_footer();
