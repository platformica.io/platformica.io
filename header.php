<!DOCTYPE html>
<!--

    ██████╗ ██╗      █████╗ ████████╗███████╗ ██████╗ ██████╗ ███╗   ███╗██╗ ██████╗ █████╗    ██╗ ██████╗
    ██╔══██╗██║     ██╔══██╗╚══██╔══╝██╔════╝██╔═══██╗██╔══██╗████╗ ████║██║██╔════╝██╔══██╗   ██║██╔═══██╗
    ██████╔╝██║     ███████║   ██║   █████╗  ██║   ██║██████╔╝██╔████╔██║██║██║     ███████║   ██║██║   ██║
    ██╔═══╝ ██║     ██╔══██║   ██║   ██╔══╝  ██║   ██║██╔══██╗██║╚██╔╝██║██║██║     ██╔══██║   ██║██║   ██║
    ██║     ███████╗██║  ██║   ██║   ██║     ╚██████╔╝██║  ██║██║ ╚═╝ ██║██║╚██████╗██║  ██║██╗██║╚██████╔╝
    ╚═╝     ╚══════╝╚═╝  ╚═╝   ╚═╝   ╚═╝      ╚═════╝ ╚═╝  ╚═╝╚═╝     ╚═╝╚═╝ ╚═════╝╚═╝  ╚═╝╚═╝╚═╝ ╚═════╝

    Hey there, earthling!  If you like Linux containers and
    are interested in making the internet a better,
    faster place then we should probably chat.
    Check out https://www.platformica.io/

-->

<html class="no-js" <?php language_attributes(); ?>>

    <head>
        <meta charset="<?php bloginfo('charset'); ?>">

        <title></title>

        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php wp_head(); ?>

        <!--<link rel="manifest" href="site.webmanifest">-->
        <link rel="apple-touch-icon" href="icon.png">

        <meta name="theme-color" content="#fafafa">
    </head>

    <body <?php body_class(); ?>>

        <header id="header">
            <nav>
                <div class="grid-container fluid">
                    <div class="grid-x navs">
                        <div class="cell shrink">
                            <div class="component logo">
                                <a href="<?php echo get_home_url(); ?>"><?php include('images/svg/logo.svg'); ?></a>
                            </div>
                        </div>
                        <div class="cell auto">
                        </div>
                        <div class="cell shrink hid-for-small-only">
                            <div class="component nav hamburger">
                                <?php wp_nav_menu(['theme_location' => 'primary','menu_class' => 'primary-menu', 'container' => 'ul']); ?>
                            </div>
                        </div>
                        <div style="display: flex; justify-content: center; align-items: center;" class="cell shrink show-for-small-only">
                            <div class="component hamburger">
                                <a href="#"><?php include('images/svg/menu.svg'); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </header>

        <main>
