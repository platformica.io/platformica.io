module.exports = function(grunt) {
	"use strict";

	const sass = require('node-sass');

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		sass: {
			options: {
				implementation: sass,
				includePaths: [
					'node_modules/'
				],
				sourceMap: false,
				outputStyle: 'nested',
			},
			default: {
				files: {
					'assets/platformica.css': ['sass/platformica.sass']
				}
			}
		},

		concat: {
			vendor: {
				files: {
					'assets/vendor.js': [
						"node_modules/canvas-5-polyfill/canvas.js",
						"node_modules/jquery/dist/jquery.js",
//
						"node_modules/d3-array/dist/d3-array.js",
						"node_modules/d3-color/dist/d3-color.js",
						"node_modules/d3-dispatch/dist/d3-dispatch.js",
						"node_modules/d3-ease/dist/d3-ease.js",
						"node_modules/d3-fetch/dist/d3-fetch.js",
						"node_modules/d3-geo/dist/d3-geo.js",
						"node_modules/d3-interpolate/dist/d3-interpolate.js",
						"node_modules/d3-path/dist/d3-path.js",
						"node_modules/d3-selection/dist/d3-selection.js",
						"node_modules/d3-shape/dist/d3-shape.js",
						"node_modules/d3-timer/dist/d3-timer.js",
						"node_modules/d3-transition/dist/d3-transition.js",


						//"node_modules/d3/dist/d3.js",

						"node_modules/topojson-client/dist/topojson-client.js",
						"node_modules/swiper/dist/js/swiper.js"
					]
				}
			},
			platformica: {
				files: {
					'assets/platformica.js' : [
						'sjs/square.js',
						'sjs/circle.js',
						'sjs/track.js',
						'sjs/platformica.js'
					]
				}
			}
		},

		terser: {
			vendor: {
				files: {
					'assets/vendor.min.js': ['assets/vendor.js']
				}
			},
			platformica: {
				files: {
					'assets/platformica.min.js': ['assets/platformica.js']
				}
			}
		},
		cssmin: {
			default: {
				files: {
					'assets/platformica.min.css': ['assets/platformica.css'],

				}
			}
		},
		clean: {
			default: []
		},

		postcss: {
			options: {
				map: true,
				processors: [
					require('autoprefixer')
				]
			},
			dist: {
				src: 'assets/platformica.css'
			}
		},

		watch: {
			options: {
				reload: true
			},
			default: {
				files: ['Gruntfile.js'],
				tasks: ['default']
			},
			sass: {
				files: ['sass/**/*.sass'],
				tasks: ['rsass']
			},
			js: {
				files: ['sjs/**/*.js'],
				tasks: ['rjs']
			}
		}
	});

	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-terser');
	grunt.loadNpmTasks('grunt-contrib-cssmin');

	grunt.loadNpmTasks('grunt-postcss');

	grunt.registerTask('default', ['clean','sass','postcss','cssmin','concat:vendor','concat:platformica','terser:vendor','terser:platformica','watch']);
	grunt.registerTask('rjs', ['concat:platformica','terser:platformica']);
	grunt.registerTask('rsass', ['sass','postcss','cssmin']);
}
