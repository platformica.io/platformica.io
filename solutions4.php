<?php

/**
 * Template Name: Platformica #solutions4
 *
 * @package platformica
 */

?>
<?php get_header(); ?>

<?php
    $solutions = [getData()['solutions'][0],getData()['solutions'][1],getData()['solutions'][2]];



?>

<svg style="max-width:1500px" data-square='{ "image" : "https://www.platformica.io/wp-content/themes/platformica/images/png/d.png" , "threshold" : 0.1 , "pixel" : 2 , "gutter" : 2 , "width" : 1500 }' ></svg>

<section id="solution-4">
    <div class="grid-container">
        <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12">
                <div class="component heading">
                    <h1>Telco multi-cloud for NFV and IT apps</h1>
                    <h4>Telco cloud solutions embracing the automation approach</h4>
                    <hr />
                </div>
            </div>
        </div>
    </div>
</section>
<section id="solution-4-1" class="general">
    <div class="grid-container">
        <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12 medium-6">
                <div class="component text styled">
                    <h4>Understanding differenet NFV approaches</h4>
                    <p>Some operators are building standards driven platforms (typically ETSI MANO). Other operators are choosing more organic and lightweight approach for their cloud strategy by using disaggregated components and automating their lifecycle and operations using automation tools. One of the key benefits of the automation approach is its ability to build completely disaggregated infrastructures.  Different physical hardware, different VNFs, different networking fabrics and overlays, different virtualization platforms and different OSS and BSS software components can all be integrated to a tailor made solution. </p>
                </div>
            </div>
            <div class="cell small-12 medium-6">
                <div class="component splashimage">
                    <div class="grid-x">
                        <div class="cell shrink">
                            <?php include("images/svg/cloud-blue.svg"); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="grid-container">
        <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12">
                <div class="component text styled">
                    <h4>Embracing the automation approach</h4>
                    <p>Using the automation approach we help operators to go business case by business case and organically build integrated multi-cloud solution with infrastructure “verticals” optimized for specific workload. The Telco/NFV part of the  infrastructure which is purpose built for Telco/NFV to be optimal and economical for running Telco/NFV workloads (for example by focusing more on networking and less on storage aspects of the infrastructure) while the IT part of the infrastructure is purpose built for IT workload.</p>
                </div>
            </div>
        </div>

        <div class="grid-x" style="align-items: center;">
            <div class="cell small-12 medium-7">
                <div data-avoid class="component text styled" >
                    <h4>Embracing heterogeneous cloud</h4>
                    <p data-avoid >Check out our following disaggregated infrastructure solutions to see how Platformica can build comprehensive multi-cloud solution from small and simple  infrastructure clouds based on popular platforms such as VMware, OpenStack, Kubernetes and automated bare metal.</p>
                </div>
                <div class="component bullets hide-for-small-only">
                    <br /><br />
                    <div class="grid-x">
                        <div class="cell shrink">
                            <div data-avoid class="pad"><svg data-track='{ "link" : "002", "offset" : [[0,0],[20,135],[56,45],[250,0]] }' data-pad="{}" /><?php echo $solutions[0]->title; ?></div>
                        </div>
                        <div class="cell shrink">
                            <div data-avoid class="pad"><svg data-track='{ "link" : "001", "offset" : [[0,0],[20,235],[56,315],[250,0]] }' data-pad="{}" /><?php echo $solutions[2]->title; ?></div>
                        </div>
                        <div class="cell shrink">
                            <div data-avoid class="pad"><svg data-track='{ "link" : "003", "offset" : [[0,0],[20,235],[56,315],[250,0]] }' data-pad="{}" /><?php echo $solutions[1]->title; ?></div>
                        </div>
                    </div>
                    <br /><br />
                </div>


                <div class="grid-x grid-margin-x  small-up-1 medium-up-2 large-up-3 show-for-small-only">
                    <?php foreach($solutions as $solution): ?>
                    <div class="cell">
                        <div class="component solution">
                            <div class="grid-x">
                                <div class="cell small-12 medium-7">
                                    <div class="overlay" style="background: url('<?php echo get_template_directory_uri(); ?>/<?php echo $solution->src; ?>') center center/cover;">
                                        <div class="content">
                                            <h5><?php echo $solution->title; ?></h5>
                                            <a class="button" href="<?php echo $solution->href; ?>">Read More</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="cell small-12 medium-5 hide-for-small-only">
                                    <div class="component text">
                                        <p><?php echo $solution->text; ?></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>


                <div data-avoid class="component text styled" >
                    <p  >Check out our Realistic NFV paper and also our Heteregenout network presentation to btter understand benefits of disaggregation and automation approach and more technical details how hetereogenous multi-cloud for Telco/IT workdload can be built in down-to-earth way.</p>
                    <ul>
                        <li><?php include('images/svg/next.svg'); ?><a href="">Realistic NFV paper</a></li>
                        <li><?php include('images/svg/next.svg'); ?><a href="">Heteregenout network presentation</a></li>
                    </ul>
                </div>
            </div>
            <div class="cell small-12 medium-5 hide-for-small-only">
                <div class="component nextsolution">
                    <div class="grid-x">
                        <div class="cell small-12 medium-12 large-6">
                            <div class="grid-x" style="justify-content: flex-end;">
                                <div  class="cell small-12" style="max-width: 250px;">
                                    <div  data-avoid class="overlay" style="background: url('<?php echo get_template_directory_uri(); ?>/<?php echo $solutions[0]->src; ?>') center center/cover;"><svg data-track='{ "link" : "002", "offset" : [[0,0],[48,180]]  }' data-pad='{ "width" : "45", "height" : "45" }' /></div>
                                </div>
                                <div  class="cell small-12" style="max-width: 250px;">
                                    <div  data-avoid  class="overlay" style="background: url('<?php echo get_template_directory_uri(); ?>/<?php echo $solutions[1]->src; ?>') center center/cover;"><svg data-track='{ "link" : "003", "offset" : [[0,0],[48,180]] }' data-pad='{ "width" : "45", "height" : "45" }' /></div>
                                </div>
                            </div>
                        </div>
                        <div class="cell small-12 medium-12 large-6">
                            <div class="grid-x" style="justify-content: flex-end;">
                                <div  class="cell small-12" style="max-width: 250px;">
                                    <div data-avoid  class="overlay" style="background: url('<?php echo get_template_directory_uri(); ?>/<?php echo $solutions[2]->src; ?>') center center/cover;"><svg data-track='{ "link" : "001", "offset" : [[0,0],[48,180]]  }' data-pad='{ "width" : "45", "height" : "45" }' /></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




<?php get_footer();

