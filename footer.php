



        </main>
        <footer id="footer">
            <div class="grid-container">
                <div class="grid-x">
                    <div class="cell small-12 medium-3">
                        <div class="component smaller">
                            <h6>Solutions</h6>
                            <ul>
                                <?php foreach(getData()['solutions'] as $solution): ?>
                                    <li><a style="color:inherit; text-decoration: none;" href="<?php echo $solution->href; ?>" /><?php echo $solution->title; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                           <h6>Services</h6>
                            <ul>
                                <?php foreach(getData()['services'] as $service): ?>
                                    <li><a style="color:inherit; text-decoration: none;" href="<?php echo $service->href; ?>" /><?php echo $service->title; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                           <h6>Appliances</h6>
                            <ul>
                                <?php foreach(getData()['appliances'] as $appliance): ?>
                                    <li><a style="color:inherit; text-decoration: none;" href="<?php echo $appliance->href; ?>" /><?php echo $appliance->title; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                    <div class="cell small-12 medium-3">
                        <div class="component smaller">
                           <h6>Company</h6>
                            <ul>
                                <li>Who we are?</li>
                                <li>Our vision</li>
                                <li>Customers</li>
                                <li>Careers</li>
                            </ul>
                           <h6>Technical support</h6>
                            <ul>
                                <li>Slack</li>
                                <li>Customer portal</li>
                                <li><a href="/careers/">Career</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="cell small-6 medium-3">
                        <div class="component smaller">
                            <h6>Address</h6>

                            <p>Platformica s.r.o.<br />
                            Na Strži 1702/65,<br />
                            140 62 Praha 4</p>

                            <p>ICO: 00000000<br />
                            DIC CZ00000000</p>
                        </div>
                    </div>
                    <div class="cell small-6 medium-3">
                        <div class="component smaller">
                            <h6>Support</h6>
                            <p>+420 000 000 000<br />
                            support@platformica.io</p>

                            <h6>Sales</h6>
                            <p>+420 000 000 000<br />
                            sales@platformica.io</p>

                            <h6>Inquiries</h6>
                            <p>+420 000 000 000<br />
                            info@platformica.io</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="grid-container fluid">
                <div class="grid-x">
                    <div class="cell small-12">
                        <div class="component smaller">
                            <p>© 2019 Platformica.io</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <?php wp_footer(); ?>


        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-133305162-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-133305162-1');
        </script>

        <div id="debug">
        </div>
    </body>
</html>














