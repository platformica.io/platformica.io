<?php


function getData() {


    $solutions = [];


    $solutions[] = (object) [
        'src' => 'images/jpg/kubernetes-public-private-clusters.jpg',
        'title' => 'VMware infrastructure for traditional IT',
        'href' => '/vmware-infrastructure-for-traditional-it/',
        'text' => 'Platformica is a suitable partner especially for customers who believe the VMware is the best platform for legacy workload but want to keep cloud-native and containerized workload outside of VMware ecosystem. '
    ];

    $solutions[] = (object) [
        'src' => 'images/jpg/openstack-based-datacenter-infrastructure.jpg',
        'title' => 'Minimalist OpenStack for cloud-native apps',
        'href' => '/minimalist-openstack-for-cloud-native-apps/',
        'text' => 'OpenStack is the most popular open source platform for building cloud-native infrastructure.'
    ];

    $solutions[] = (object) [
        'src' => 'images/jpg/bare-metal-automation.jpg',
        'title' => 'Integrated Kubernetes infrastructure',
        'href' => '/integrated-kubernetes-infrastructure/',
        'text' => 'OpenStack is the most popular open source platform for building cloud-native infrastructure.'
    ];

    $solutions[] = (object) [
        'src' => 'images/jpg/vmware-vsphere-and-vmware-vcloud.jpg',
        'title' => 'Telco multi-cloud for NFV and IT apps',
        'href' => '/telco-multi-cloud-for-nfv-and-it-apps/',
        'text' => 'One of the key benefits of the automation approach is its ability to build completely disaggregated infrastructures.'
    ];

    $solutions[] = (object) [
        'src' => 'images/jpg/bare-metal-automation.jpg',
        'title' => 'Bare metal cloud',
        'href' => '/bare-metal-cloud/',
        'text' => 'Physical server, network and storage infrastructure are foundation for any application platform whether it is a low level layer for private cloud based.'
    ];


    $services = [];

    $services[] = (object) [
        'src' => 'images/png/160x160/buildandsupport.png',
        'title' => 'Build & Support',
        'href' => '/services/',
        'text' => 'We Build and support your application infrastructures. Automate all integration points and automate all relevant processes (e.g. CD/CI).'
    ];
    $services[] = (object) [
        'src' => 'images/png/160x160/migrateandcustomize.png',
        'title' => 'Migrate & customize',
        'href' => '/services/',
        'text' => 'We Migrate existing applications to new infrastructure environments and implement necessary customizations. We are experts in migration from public cloud to private cloud and vice versa.'
    ];
    $services[] = (object) [
        'src' => 'images/png/160x160/coachandknowledge.png',
        'title' => 'Coach & knowledge transfer',
        'href' => '/services/',
        'text' => 'We transfer as much knowledge as possible to your teams leaving you and not your vendor in charge of your business.'
    ];
    $services[] = (object) [
        'src' => 'images/png/160x160/planandevolve.png',
        'title' => 'Plan & Evolve',
        'href' => '/services/',
        'text' => 'We continuously work with your organization and align the evolution of the infrastructure with the evolution of your business.'
    ];


    $appliances = [];

    $appliances[] = (object) [
        'src' => 'images/jpg/kubernetes-public-private-clusters.jpg',
        'title' => 'Platformica NAS appliance',
        'href' => '#platformica-nas-appliance',
        'text' => 'Platformica NAS appliance is a Platformica appliance cluster which provides NAS storage for file oriented access.'
    ];

    $appliances[] = (object) [
        'src' => 'images/jpg/openstack-based-datacenter-infrastructure.jpg',
        'title' => 'Platformica LoadBalancer appliance',
        'href' => '#platformica-loadbalancer-appliance',
        'text' => 'Platformica Load Balancer appliance is a Platformica appliance cluster which provides Elastic load balancing capabilities primarily for HTTP HTTPS traffic.'
    ];

    $appliances[] = (object) [
        'src' => 'images/jpg/bare-metal-automation.jpg',
        'title' => 'Platformica router/firewall appliance',
        'href' => '#platformica-router-appliance',
        'text' => 'Platformica router appliance is a Platformica appliance which provides L3 routing, firewalling and peering capabilities.'
    ];

    $appliances[] = (object) [
        'src' => 'images/jpg/vmware-vsphere-and-vmware-vcloud.jpg',
        'title' => 'Platformica PostgreSQL appliance',
        'href' => '#platformica-postgresql-appliance',
        'text' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
    ];


    $jobs = [];

    $jobs[] = (object) [
        'title' => 'Network Operations',
        'href' => '#',
        'text' => 'Dallas, Texas'
    ];

    $jobs[] = (object) [
        'title' => 'Cloud Architect',
        'href' => '#',
        'text' => 'New York'
    ];

    $jobs[] = (object) [
        'title' => 'Front End Engineer',
        'href' => '#',
        'text' => 'Kansas City'
    ];

    $jobs[] = (object) [
        'title' => 'Data Center Operations',
        'href' => '#',
        'text' => 'Prague, Czechia'
    ];

    return ['solutions' => $solutions, 'services' => $services, 'appliances' => $appliances, 'jobs' => $jobs];

}

