<?php

/**
 * Template Name: Platformica #solutions2
 *
 * @package platformica
 */

?>
<?php get_header(); ?>

<svg style="max-width:1500px" data-square='{ "image" : "https://www.platformica.io/wp-content/themes/platformica/images/png/d.png" , "threshold" : 0.1 , "pixel" : 2 , "gutter" : 2 , "width" : 1500 }' ></svg>
<section id="solution-2">
    <div class="grid-container">
        <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12">
                <div class="component heading">
                    <h1>Dissagregated OpenStack solution</h1>
                    <h4></h4>
                    <hr />
                </div>
            </div>
        </div>
    </div>
</section>
<section id="solutions-2-1" class="general">
    <div class="grid-container">
        <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12 medium-6 small-order-2 medium-order-1">
                <div class="component text styled">
                    <h4>The OpenStack reality</h4>
                    <p>OpenStack is the most popular open source platform for building cloud-native infrastructure. Over the time however it has been shown that a lot of projects under the OpenStack umbrella just don’t have enough adoption and momentum to be acceptable for customers in production. Also delivering new features under the OpenStack ecosystem proves to be slow for many customers.</p>
                    <h4>Providing advanced OpenStack services</h4>
                    <p>Platformica provides alternative approach to realizing OpenStack infrastructure projects. We use the core OpenStack projects which are stable and provide core IaaS capabilities to deliver compute, network and storage suitable for consuming by cloud-native, NFV and other applications. On top of these basic resources we deploy single-purpose microservices for multi-tenant provisioning of platform level application components like firewalls, load balancers, monitoring and so on. These functions we keep outside of OpenStack ecosystem, making the whole infrastructure disaggregated and free to evolve on its own pace. We provide disaggregated alternatives for following OpenStack projects:</p>
                </div>
            </div>
            <div class="cell small-12 medium-6 small-order-1 medium-order-2">
                <div class="component splashimage">
                    <div class="grid-x">
                        <div class="cell shrink">
                            <?php include("images/svg/openstack.svg"); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="solution-2-list">
    <div class="grid-container">
        <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12">
                <div data-avoid class="component text center" style="position:relative;">
                    <svg class="hide-for-small-only" data-track='{ "link" : "005", "offset" : [[0,0],[48,225]], "test" : 1 }' data-pad='{ "width" : "45", "height" : "45" }' style="left: 0px; right: auto;" />
                    <h6 class="pad">Monitoring</h6>
                    <p style="margin-bottom: 0px;">Multi-instance service for creating production grade Prometheus appliances. Multi-instance service for creating production grade Prometheus appliances</p>
                </div>
                <div data-avoid class="component text center" style="position:relative;">
                    <svg class="hide-for-small-only" data-track='{ "link" : "003", "offset" : [[0,0],[48,315]] }' data-pad='{ "width" : "45", "height" : "45" }' style="left: auto; right: 0px;" />
                    <h6 class="pad">LoadBalancing Octavia</h6>
                    <p style="margin-bottom: 0px;">Multi-instance service for creating LoadBalancing appliances built on IPVS, HAProxy and Varnish. Multi-instance service for creating LoadBalancing appliances built on IPVS, HAProxy and Varnish</p>
                </div>
                <div data-avoid class="component text center" style="position:relative;">
                    <svg class="hide-for-small-only" data-track='{ "link" : "004", "offset" : [[0,0],[48,225]] }' data-pad='{ "width" : "45", "height" : "45" }' style="left: 0px; right: auto;" />
                    <h6 class="pad">Router/Firewall/VPN</h6>
                    <p style="margin-bottom: 0px;">Multi-instance service for creating application network edge appliances built on Linux and modern stack of nftables, FRrouting and Wireguard.</p>
                </div>
                <div data-avoid class="component text center" style="position:relative;">
                    <svg class="hide-for-small-only" data-track='{ "link" : "001", "offset" : [[0,0],[48,315]] }' data-pad='{ "width" : "45", "height" : "45" }' style="left: auto; right: 0px;" />
                    <h6 class="pad">Databases</h6>
                    <p style="margin-bottom: 0px;">Multi-instance service for creating clustered highly available instances of mariaDB and PostgreSQL. Multi-instance service for creating clustered highly available instances of mariaDB and PostgreSQL.</p>
                </div>
                <div data-avoid class="component text center" style="position:relative;">
                    <svg class="hide-for-small-only" data-track='{ "link" : "002", "offset" : [[0,0],[48,225]] }' data-pad='{ "width" : "45", "height" : "45" }' style="left: 0px; right: auto;" />
                    <h6 class="pad">BigData</h6>
                    <p style="margin-bottom: 0px;">Multi-instance service for creating instances of clustered Big Data appliances with scale-out characteristics based on (Hadoop, MongoDB)</p>
                </div>
            </div>
        </div>

        <div class="grid-x  grid-margin-x grid-margin-y">
            <div class="cell  small-12" style="position: relative;">
                <br class="hide-for-small-only" /><br class="hide-for-small-only" /><br /><br /><br />
                <div class="maluvka grid-x" style="justify-content: center; justify-content: center;">
                    <div class="cell shrink">
                        <div>
                            <div data-track='{ "link" : "005", "offset" : [[0,0],[48,90],[32,135],[144,180],[160,135]] }' style="background:#6bcb92;" class="overlay"></div>
                        </div>
                    </div>
                    <div class="cell shrink">
                        <div>
                            <div data-track='{ "link" : "004", "offset" : [[0,0],[48,90],[48,135],[200,180],[152,135]] }' style="background:#7fc2cf;" class="overlay"></div>
                        </div>
                    </div>
                    <div class="cell shrink">
                        <div>
                            <div data-track='{ "link" : "002", "offset" : [[0,0],[48,90],[64,135],[256,180],[144,135]] }' style="background:#7e6bcb;" class="overlay"></div>
                        </div>
                    </div>
                    <div class="cell shrink">
                        <div>
                            <div data-track='{ "link" : "001", "offset" : [[0,0],[48,90],[48,45],[176,0],[160,45],[16,90]] }' style="background:#f0f4f7;" class="overlay"></div>
                        </div>
                    </div>
                    <div class="cell shrink">
                        <div>
                            <div data-track='{ "link" : "003", "offset" : [[0,0],[48,90],[32,45],[120,0],[168,45]] }' style="background:#cb746b;" class="overlay"></div>
                        </div>
                    </div>
                </div>
                <div class="maluvka2 grid-x" style="justify-content: center;">
                    <div class="cell auto">
                        <div style="max-width: 450px;margin: auto;">
                            <div style="background: url('<?php echo get_template_directory_uri(); ?>/images/png/solution2.png') center center/contain no-repeat;" class="overlay"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php get_footer();

