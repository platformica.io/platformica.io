(function (root, factory) {
if (typeof define === 'function' && define.amd) {define([], factory);
} else if (typeof module === 'object' && module.exports) {
module.exports = factory();} else { root.Track = factory();}
}(typeof self !== 'undefined' ? self : this, function () {
    var TrackClass = function TrackClass(options) {

        var self = this;
        self.options = options;

        var tracks = [];
        var banned = [];

        var _step = 96;
        var _gutter = 4;
        var _angles = [0, 45, 90, 135, 180, 225, 270, 315];

        var color = [];
        color['001'] = 'blue';
        color['002'] = 'red';
        color['003'] = 'yellow';

        var svg = d3.select("body").append("svg").attr('id','track');


        self.intersect =  function hasIntersection(v1,v2) {


            var x1 = v1[0][0];
            var y1 = v1[0][1];
            var x2 = v1[1][0];
            var y2 = v1[1][1];
            var x3 = v2[0][0];
            var y3 = v2[0][1];
            var x4 = v2[1][0];
            var y4 = v2[1][1];

            function RotationDirection(p1x, p1y, p2x, p2y, p3x, p3y)
            {
                if (((p3y - p1y) * (p2x - p1x)) > ((p2y - p1y) * (p3x - p1x))) {
                    return 1;
                }
                else if (((p3y - p1y) * (p2x - p1x)) == ((p2y - p1y) * (p3x - p1x))) {
                    return 0;
                }

                return -1;
            }

            function containsSegment(x1, y1, x2, y2, sx, sy)
            {
                if (x1 < x2 && x1 < sx && sx < x2) {
                    return true;
                }
                else if (x2 < x1 && x2 < sx && sx < x1) {
                    return true;
                }
                else if (y1 < y2 && y1 < sy && sy < y2) {
                    return true;
                }
                else if (y2 < y1 && y2 < sy && sy < y1) {
                    return true;
                }
                else if (x1 == sx && y1 == sy || x2 == sx && y2 == sy) {
                    return true;
                }

                return false;
            }

            var f1 = RotationDirection(x1, y1, x2, y2, x4, y4);
            var f2 = RotationDirection(x1, y1, x2, y2, x3, y3);
            var f3 = RotationDirection(x1, y1, x3, y3, x4, y4);
            var f4 = RotationDirection(x2, y2, x3, y3, x4, y4);

            var intersect = f1 != f2 && f3 != f4;

            if (f1 == 0 && f2 == 0 && f3 == 0 && f4 == 0) {
                intersect = containsSegment(x1, y1, x2, y2, x3, y3) || containsSegment(x1, y1, x2, y2, x4, y4) || containsSegment(x3, y3, x4, y4, x1, y1) || containsSegment(x3, y3, x4, y4, x2, y2);
            }

            return intersect;
        }

        self.isIntersect = function(v)
        {
            var aa = avoid.slice(0,-8);

            for(i in aa) {
                var a = avoid[i];
                var intersect = self.intersect(v,a);

                if(intersect) {
                    return true;
                }
            }

            return false;
        }

        self.angle = function(v){
            var degrees =  Math.atan2(v[1][1] - v[0][1], v[1][0] - v[0][0]) * 180 / Math.PI;
            return (360+Math.round(degrees))%360;
        }

        self.sort = function(unordered)
        {
            var ordered = {};
            Object.keys(unordered).sort().forEach(function(key) {
              ordered[key] = unordered[key];
            });
            return ordered;
        }

        self.step = function(c,a,s)
        {

            x = Math.round(Math.cos(a * Math.PI / 180) * s + c[0]);
            y = Math.round(Math.sin(a * Math.PI / 180) * s + c[1]);
            return [x, y];
        }


        self.distance = function(c1,c2)
        {

            return Math.round(Math.hypot(c2[0]-c1[0], c2[1]-c1[1]));
        }


        self.avoid = function(v)
        {
            var angle = self.angle(v);

            var a1 =(360+angle+90)%360;
            var a2 = (360+angle-90)%360;


            avoid.push(v);

            avoid.push([self.step(v[0],a1,_gutter),self.step(v[0],a2,_gutter)]);
            avoid.push([self.step(v[0],a2,_gutter),self.step(v[1],a2,_gutter)]);
            avoid.push([self.step(v[1],a2,_gutter),self.step(v[1],a1,_gutter)]);
            avoid.push([self.step(v[1],a1,_gutter),self.step(v[0],a1,_gutter)]);

        }

        self.tracing = function(c1, c2, path = [], _d = 0)
        {

            if(_d++ > 100) {
                return [];
            }

            var c = self.strategyAvoidance(c1, c2, path);

            path.push(c);

            var vector = path.slice(Math.max(path.length - 2));
            self.avoid([[vector[0][0],vector[0][1]],[vector[1][0],vector[1][1]]]);


            if (self.distance(c,c2) > 0){
                self.tracing(c,c2,path,_d);
            }


            return path;
        }

        self.strategyAvoidance = function(c1,c2,path)
        {
            var steps = [];

            for(i in _angles) {
                var angle = _angles[i];
                var step = self.step(c1, angle, _step);
                var distance = self.distance(step,c2);

                var d = [Math.round(Math.abs(c1[0] - c2[0])),Math.round(Math.abs(c1[1] - c2[1]))];
                var s = [];

                if((d[0] < _step) && (d[0] > 0)) {
                    var denominator = Math.cos(angle * Math.PI / 180);
                    var numerator = c1[0] - c2[0];
                    denominator = ((angle == 90) || (angle == 270)) ? 1 : denominator;
                    s[0] = Math.round(Math.abs(numerator/denominator));
                }

                if((d[1] < _step) && (d[1] > 0)) {
                    var denominator = Math.sin(angle * Math.PI / 180);
                    var numerator = c1[1] - c2[1];
                    denominator = ((angle == 180) || (angle == 360) || (angle == 0)) ? 1 : denominator;
                    s[1] = Math.round(Math.abs(numerator/denominator));
                }

                if(s.length) {
                    s.sort(function(a, b){return a-b});
                    step = self.step(c1, angle, s[0]);
                }

                step[10] = 0;

                var d = [Math.round(Math.abs(step[0] - c2[0])),Math.round(Math.abs(step[1] - c2[1]))];

                if(d[0] == 0) {
                    step[10] = step[10] - 100;
                }
                if(d[1] == 0) {
                    step[10] = step[10] - 100;
                }

                var intersect = self.isIntersect([[c1[0],c1[1]],[step[0],step[1]]]);

                var intersect = (intersect) ? 100000 : 0;
                step[2] = angle;
                step[3] = distance;

                d = step[2] - ((path[path.length-1]) ? path[path.length-1][2] : 0);
                d = Math.abs((d + 180) % 360 - 180);

                step[4] = d;
                step[10] = step[10] + distance + intersect;
                step[10] = step[10] + d/10;

                //console.log(step);

                steps.push(step);
            }

            steps = steps.sort(function(a, b) {
                if (a[10] > b[10]) return 1;
                if (b[10] > a[10]) return -1;
            });


            return steps[0];
        }

        self.resize = function()
        {
            var lineGenerator = d3.line().x(function(d) { return d[0]; }).y(function(d) { return -1*d[1]; });

            avoid = [];
            tracks = [];
            width = height = 0;

            d3.selectAll('[data-avoid]').each(function(d,i){

                var pad = d3.select(this);
                var box = pad.node().getBoundingClientRect();

                    box.x = (box.x + window.scrollX)*1;
                    box.y = (box.y + window.scrollY)*-1;

                self.avoid([[box.x, box.y], [box.x + box.width, box.y]]);
                self.avoid([[box.x + box.width, box.y], [box.x + box.width, box.y - box.height]]);
                self.avoid([[box.x + box.width, box.y - box.height], [box.x, box.y - box.height]]);
                self.avoid([[box.x, box.y - box.height], [box.x, box.y]]);

            });


            d3.selectAll('[data-track]').each(function(d,i){
                var data = d3.select(this).attr('data-track');
                data = JSON.parse(data);
                if(!tracks[data.link]) {
                    tracks[data.link] = []
                }
                tracks[data.link].push(this);
            });

            tracks = self.sort(tracks);


            svg.selectAll('path').remove();
            svg.selectAll('rect').remove();

            for (it in tracks) {
                var track = tracks[it];

                var polygon = [];

                track.forEach(function(t) {
                    var pad = d3.select(t);
                    var data = pad.attr('data-track');
                    data = JSON.parse(data);

                    var box = pad.node().getBoundingClientRect();
                        box.x = (box.x + window.scrollX)*1;
                        box.y = (box.y + window.scrollY)*-1;

                    var x = Math.round(box.x + box.width/2);
                    var y = Math.round(box.y - box.height/2);

                    var subpolygon = [];
                    var c = [x,y];

                    for(i in data.offset) {
                        var o = data.offset[i];
                        var step = self.step(c,o[1],o[0]);

                        subpolygon.push([step[0],step[1],o[1]]);
                        c = [step[0],step[1]];
                    }

                    if(polygon.length) {
                        subpolygon.reverse();
                        var c1 = polygon.slice(-1)[0];
                        var c2 = subpolygon[0];
                        self.tracing(c1,c2,polygon);
                    }

                    polygon = polygon.concat(subpolygon);

                    var c = false;
                    for (i in polygon) {
                        if(c) {
                            self.avoid([c,polygon[i]]);
                        }
                        c = polygon[i];
                    }

                });

                svg.append("path").attr("d", lineGenerator(polygon))

                for (i in polygon) {
                    var d = polygon[i];
                    width = Math.max(width,Math.abs(d[0]));
                    height = Math.max(height,Math.abs(d[1]));
                    //svg.append("rect").attr("x",d[0]).attr("y",-1*d[1]).attr("width",3).attr("height",3).attr("fill",color[it]);
                }
                for (i in avoid) {
                    var a = avoid[i];

                    //svg.append("path").attr("d", lineGenerator(a));

                }




            }

            svg.attr("width",width+_gutter).attr("height",height+_gutter);
        }





    };





    return TrackClass;
}));


