(function (root, factory) {
if (typeof define === 'function' && define.amd) {define([], factory);
} else if (typeof module === 'object' && module.exports) {
module.exports = factory();} else { root.Square = factory();}
}(typeof self !== 'undefined' ? self : this, function () {



function getImageData(imageData, x, y, w, h){
    var i,j;
    var result = "";
    var r,g,b,a;
    const data = imageData.data;

    for(j = 0; j < h; j++){
        var idx = (x + (y + j) * imageData.width) * 4;  // get left most byte index for row at y + j
        for(i = 0; i < w; i++){
             r = data[idx ++];
             g = data[idx ++];
             b = data[idx ++];
             a = data[idx ++];

        }
     }
     return result;
 }


    var SquareClass = function SquareClass(object,options) {
        var self = this;

        self.options = options;
        self.svg = {
            'pattern' : d3.select('body').append('svg').style('display','none'),
            'canvas' : d3.select('body').append('canvas').style('display','none'),
            'svg' : d3.select(object)
        };


        self.getImageData = function(imageData, x, y, w, h) {
            var i,j;
            var r,g,b,a;
            const data = imageData.data;

            var datay = {};
            for(j = 0; j < h; j++){
                var idx = (x + (y + j) * imageData.width) * 4;

                var datax = {};
                for(i = 0; i < w; i++){
                     r = data[idx ++];
                     g = data[idx ++];
                     b = data[idx ++];
                     a = data[idx ++];
                     datax[i] = [r,g,b,a];
                }
                datay[j] = datax;

             }

             return datay;
         }

        self.createCanvasfromTopology = function(data) {
            self.svg.pattern.attr('width', 825).attr('height', 425);
            var projection = d3.geoPath().projection(d3.geoNaturalEarth1().scale(175).translate([375,250]));
            var context = self.svg.canvas.node().getContext('2d');
            self.svg.canvas.attr('width',825).attr('height',425);

            var path = self.svg.pattern
                .append("path")
                .datum(data)
                .attr("d", projection);

            context.fill(new Path2D(path.attr("d")));
        }

        self.createCanvasfromImage = function(image) {
            var context = self.svg.canvas.node().getContext('2d');
            self.svg.canvas.attr('width',image.width).attr('height',image.height);
            context.drawImage(image, 0, 0);
        }

        self.renderFromCanvas = function() {

            var context = self.svg.canvas.node().getContext('2d');

            var width = Math.min($(self.svg.svg.node()).width(),context.canvas.width)
            var height = Math.min($(self.svg.svg.node()).height(),context.canvas.height)

            var rgbas = self.getImageData(context.getImageData(0,0,width,height),0,0,width, height);
            var data = [];




            var pixel = self.options.pixel*Math.min(1, 1/window.devicePixelRatio);
            var gutter = self.options.gutter*Math.min(1, 1/window.devicePixelRatio);
                pixel = Math.round(pixel);
                gutter = Math.round(gutter);

            for (var y = 0; y < height; y += (gutter + pixel)) {
                for (var x = 0; x < width; x += (gutter + pixel)) {
                    rgba = [rgbas[y][x][0],rgbas[y][x][1],rgbas[y][x][2],(1/255)*rgbas[y][x][3]];
                    if(rgba[3] > self.options.threshold) {
                        data.push([x,y,rgba]);
                    }
                }
            }

            //self.svg.svg.attr('width',context.canvas.width).attr('height',context.canvas.height);
            //self.svg.svg.append('defs').append('pattern').attr('id','img1').attr("patternUnits","userSpaceOnUse").attr('width',4).attr('height',4).append('image')
            //.attr('xlink:href',"http://platformica.io/wp-content/themes/platformica/images/png/test.jpg")
            //.attr('x',0).attr('y',0).attr('width',4).attr('height',4);




            self.svg.svg.selectAll('rect').remove();
            self.svg.svg.selectAll('rect')
                .data(data)
                .enter()
                .append('rect')
                .attr('width', pixel)
                .attr('height', pixel)
                .attr('x', function(d){ return d[0]; })
                .attr('y', function(d){ return d[1]; })
                .attr('opacity',1)
                .attr('fill',function(d){ return (self.options.fill) ? self.options.fill : 'rgba('+d[2].join(",")+')';  });



        }

        self.resizeCanvas = function(width,height) {
            var context = self.svg.canvas.node().getContext('2d');
            var tmpContext = document.createElement("canvas").getContext("2d");

            width = width;
            height = (height) ? height : (width/self.svg.canvas.attr('width'))*self.svg.canvas.attr('height');

            //clone image to tmpContext/tmpCanvas
            tmpContext.canvas.width = context.canvas.width;
            tmpContext.canvas.height = context.canvas.height;
            tmpContext.drawImage(context.canvas,0,0,context.canvas.width,context.canvas.height,0,0,context.canvas.width,context.canvas.height);

            //draw scaled tmpContext to context
            context.canvas.width = width;
            context.canvas.height = height;
            context.drawImage(tmpContext.canvas,0,0,tmpContext.canvas.width,tmpContext.canvas.height,0,0,width,height);
        }

        self.resize = function(width,height) {


            var t0 = performance.now();

            var context = self.svg.canvas.node().getContext('2d');

            if(self.options.data) {
                self.createCanvasfromTopology(topojson.feature(self.options.data,self.options.data.objects.land));
            } else if (self.options.image) {
                self.createCanvasfromImage(self.options.image);
            }

            self.resizeCanvas(width,height);

            var t1 = performance.now();
            console.log("#1 Call to doSomething took " + (t1 - t0) + " milliseconds.");

            self.renderFromCanvas();

            //self.svg.pattern.select('path').remove();
            context.clearRect(0, 0, width, height);



        };

    };
    return SquareClass;

}));

