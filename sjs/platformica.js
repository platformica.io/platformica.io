var resizeCallbacks = [];
var resizeQueue = [];
var size = [];

$(document).ready(function() {

    size = [$(window).width(),$(window).height()];

    //overlay menu
    $('.hamburger > a').on("click",function(){
        $('.hamburger').toggleClass("on");
    });


    //data-square
    $('[data-square]').each(function(){

        if(!$('[data-square').is(":visible")) {
            return true;
        }

        var data = $(this).data('square');
        var object = $(this);

        if(data.data) {
            d3.json(data.data).then(function(_data) {
                data.data = _data;
                var square = new Square(object[0],data);

                resizeCallbacks.push(function(){
                    square.resize(document.body.clientWidth);
                });

                square.resize(document.body.clientWidth);

            });
        }

        if(data.image) {
            var image = new Image();
            image.onload = function(){
                data.image = image;
                var square = new Square(object[0],data);

                resizeCallbacks.push(function(){
                    square.resize(data.width);
                });

                square.resize(data.width);
            }

            image.src = data.image;
        }
    });

    //circle homepage
    var circle = new Circle({});

    resizeCallbacks.push(function(){
        circle.resize(document.body.clientWidth);
    });
    circle.resize(document.body.clientWidth);

    //testimonial slider
    var testimonials = new Swiper('[data-testimonials]', {
        slidesPerView: 1,
        spaceBetween: 0,
        slidesPerGroup: 1,
        loop: true,
        loopFillGroupWithBlank: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true
        }
    });

    testimonials.on('transitionEnd', function() {
        $('[data-testimonials-title]').text($('.testimonials .swiper-slide-active [data-title]').data('title'));
        circle.update(testimonials.realIndex);
    });

    $('[data-testimonials-click]').on("click",function() {
        testimonials.slideTo($(this).data('testimonials-click').slide);
    });

    $('[data-testimonials-next]').on("click",function() {
        testimonials.slideNext();
    });


    //whitepapers slider
    var whitepapers = new Swiper('[data-whitepapers]', {
        spaceBetween: 0,
        loop: true,
        loopFillGroupWithBlank: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        },
        slidesPerView: 3,
        breakpoints: {
            900: {
                slidesPerView: 1,
            },
        }
    });


    //jobs slider
    var careers = new Swiper('[data-careers]', {
        spaceBetween: 0,
        loop: true,
        loopFillGroupWithBlank: true,
        slidesPerView: 3,
        breakpoints: {
            900: {
                slidesPerView: 1,
            },
        },
        autoplay: {
            delay: 1250,
        },
    });

    //svg pads
    $('svg[data-pad]').each(function(){
        var data = $(this).data('pad');

        if(data.height && data.width) {
            var height = data.height;
            var width = data.width;
        } else {
            var height = width = $(this).parent().outerHeight();
        }

        var arc = d3.arc().innerRadius(height/2-3).outerRadius(height/2).startAngle(0).endAngle(2 * Math.PI);
        var svg = d3.select(this).attr('width',width).attr('height',height);
        var g = svg.append("g").attr("transform", `translate(${width/2},${height/2})`);
        var path = g.append("circle").attr("r",height/2-3).attr('fill','#161818').attr('stroke-width',6).attr('stroke','#6bcb92');
    });



    //track
    var track = new Track();

    resizeCallbacks.push(function(){
        track.resize(document.body.clientWidth);
    });
    track.resize(document.body.clientWidth);

    //resize event
    $(window).on('resize',function(){
        if($(window).width() != size[0] || $(window).height() != size[1]){
            size = [$(window).width(),$(window).height()];
            var callbacks = resizeCallbacks;

            for (i in callbacks) {
                var callback = callbacks[i];
                callback();
            }
        }
    });
})
