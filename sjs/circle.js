(function (root, factory) {
if (typeof define === 'function' && define.amd) {define([], factory);
} else if (typeof module === 'object' && module.exports) {
module.exports = factory();} else { root.Circle = factory();}
}(typeof self !== 'undefined' ? self : this, function () {

    var CircleClass = function CircleClass(options) {
        var self = this;

        self.options = options;
        self.circles = {'master':[],'slave':[]};

        self.radians = function(degrees) {
            return degrees * Math.PI / 180;
        };

        self.degrees = function(radians) {
          return radians * 180 / Math.PI;
        };

        self.angle = function(radians) {
            var middle = radians;
            var start = middle - (2*Math.PI)/4/2;
            var end = middle + (2*Math.PI)/4/2;

            return {startAngle:start,endAngle:end,middle:middle};
        };

        self.triangle = function(master,slave) {
            var bbox = master.svg.node().getBoundingClientRect();
            var first = {x:bbox.left+bbox.width/2,y:bbox.top+bbox.height/2};
            var bbox = slave.svg.node().getBoundingClientRect();
            var second = {x:bbox.left+bbox.width/2,y:bbox.top+bbox.height/2};

            degrees =  Math.atan2(second.y - first.y, second.x - first.x) * 180 / Math.PI;
            d1 = (360+Math.round(degrees))%360;

            first.middle = self.radians(180) + (self.radians(90) - self.radians(d1));
            second.middle = self.radians(0) + (self.radians(90) - self.radians(d1));

            first.middle = self.radians(180) - (self.radians(90) - self.radians(d1));
            second.middle = self.radians(0) - (self.radians(90) - self.radians(d1));

            return {'master':self.angle(first.middle),'slave':self.angle(second.middle)};
        }

        self.update = function(activeIndex) {


            if (activeIndex && activeIndex <= self.circles.slave.length) {
                var triangle = self.triangle(self.circles.master[0],self.circles.slave[activeIndex-1]);
                self.circles.master[0].svg.attr('class','active');
            } else {
                var triangle = {'master':self.circles.master[0].start};
                self.circles.master[0].svg.attr('class','');
            }

            var test = $(self.circles.master[0].svg.node()).width();

            self.circles.master[0].image.transition().attr("x", activeIndex*test*-1).duration(500);

            self.circles.master.concat(self.circles.slave).forEach(function(circle,i) {

                if (i == 0) {
                    var angle = triangle.master;
                } else if(activeIndex == i) {
                    var angle = triangle.slave;
                    circle.svg.attr('class','active');
                } else {
                    var angle = circle.start;
                    circle.svg.attr('class','');
                }


                circle.cap
                    .datum({startAngle: circle.angle.startAngle, endAngle: circle.angle.endAngle})
                    .transition()
                    .duration(2000)
                    .attrTween('d', function(d) {
                        var interpolate1 = d3.interpolate(d.startAngle, angle.startAngle);
                        var interpolate2 = d3.interpolate(d.endAngle, angle.endAngle);
                        circle.angle = angle;
                        var arc = d3.arc()
                            .innerRadius(circle.svg.attr('width')/2 - self.options.border)
                            .outerRadius(circle.svg.attr('width')/2);

                        return function(t) {
                            d.startAngle = interpolate1(t);
                            d.endAngle = interpolate2(t);
                            return arc(d);
                        };
                    });
            });
        };

        self.resize = function(width,height)
        {

            self.circles = {'master':[],'slave':[]};

            //init
            d3.selectAll('svg[data-circle]').each(function(d,i){

                var data = d3.select(this).attr('data-circle');
                data = JSON.parse(data);
                var svg = d3.select(this);

                self.options.gap = Math.round(document.body.clientWidth/90);
                self.options.gap = Math.min(self.options.gap,16);
                self.options.gap = Math.max(self.options.gap,8);

                self.options.border = 2;
                self.options.divider = 3;

                var $closestRelativeParent = $(this).parents().filter(function() {
                    var $this = $(this);
                    return $this.is('body') || $this.css('position') == 'relative';
                }).slice(0,1);

                data.size = $closestRelativeParent[data.size]();


                svg.selectAll('g').remove();
                svg.attr("width", data.size)
                    .attr("height", data.size)
                    .attr('data-scale',true)
                    //.attr("style","top: -"+((data.size - $(this).parent().height())/2)+"px")
                    .append("g")
                    .attr("transform", "translate(" + data.size / 2 + "," + data.size / 2 + ")");

                var g = svg.select('g');

                var circle = g.append("path")
                    .attr("d",
                        d3.arc()
                        .innerRadius(data.size/2 - self.options.gap - self.options.border)
                        .outerRadius(data.size/2 - self.options.gap)
                        .startAngle((data.type == "master") ? 0 : (2*Math.PI)/2.75)
                        .endAngle((data.type == "master") ? (2*Math.PI) : (2*Math.PI)/1.25 + (2*Math.PI)/2.9)
                    );
                var tmp = g.append("circle").attr('r',data.size/2 - self.options.gap - self.options.border).style('fill','#161818').style('opacity',0.75);

                if (data.media) {
                    var clipPath = svg.append('g')
                        .attr("transform", "translate(" + data.size / 2 + "," + data.size / 2 + ")")
                        .append("clipPath")
                        .attr('id','hexagonal-mask')
                        .append('circle')
                        .attr('cx',data.size / 2)
                        .attr('cy',data.size / 2)
                        .attr('r',data.size/2 - self.options.gap - self.options.border);

                    var image =
                        svg.append('g').append("image")
                            .attr('xlink:href',data.media)
                            .attr('x',0)
                            .attr('width',data.size*5)
                            .attr('height',data.size)
                            .attr('clip-path','url(#hexagonal-mask)')
                            .style('opacity',1);
                }

                var cap = svg.select('g').append("path");

                self.circles[data.type].push(
                    {'svg': svg,'cap': cap,'image':image,'media':data.media,'angle':self.angle(self.radians(data.start)),'start':self.angle(self.radians(data.start))}
                );
            });



            //prepare
            self.circles.master.concat(self.circles.slave).forEach(function(circle,i) {

                circle.cap.datum({
                    startAngle: circle.angle.middle,
                    endAngle: circle.angle.middle
                })
                .transition()
                .duration(2000)
                .attrTween('d', function(d) {
                    var interpolate1 = d3.interpolate(d.startAngle, circle.start.startAngle);
                    var interpolate2 = d3.interpolate(d.endAngle, circle.start.endAngle);
                    circle.angle = circle.start;

                    var arc = d3.arc()
                        .innerRadius(circle.svg.attr('width')/2 - self.options.border)
                        .outerRadius(circle.svg.attr('width')/2);

                    return function(t) {
                        d.startAngle = interpolate1(t);
                        d.endAngle = interpolate2(t);
                        return arc(d);
                    };
                });
            });
        }
    };

    return CircleClass;

}));

