<?php

/**
 * Template Name: Platformica #appliances
 *
 * @package platformica
 */

?>
<?php get_header(); ?>


<svg style="max-width:1500px" data-square='{ "image" : "https://www.platformica.io/wp-content/themes/platformica/images/png/d.png" , "threshold" : 0.1 , "pixel" : 2 , "gutter" : 2 , "width" : 1500 }' ></svg>
<section id="appliances">
    <div class="grid-container">
        <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12">
                <div class="component heading">
                    <h1>Appliances</h1>
                    <h4>Application infrastructure solutions for enterprises and operators</h4>
                    <hr />
                </div>
            </div>
        </div>
    </div>
</section>

<section id="appliances-list">
    <div class="grid-container" >
        <div class="grid-x grid-margin-x  small-up-1 medium-up-2 large-up-3">
            <?php foreach(getData()['appliances'] as $appliance): ?>
            <div class="cell">
                <div class="component appliance">
                    <div class="grid-x">
                        <div class="cell small-12 medium-7">
                            <div class="overlay" style="background: url('<?php echo get_template_directory_uri(); ?>/<?php echo $appliance->src; ?>') center center/cover;">
                                <div class="content">
                                    <h5><?php echo $appliance->title; ?></h5>
                                    <a class="button" href="<?php echo $appliance->href; ?>">Read More</a>
                                </div>
                            </div>
                        </div>
                        <div class="cell small-12 medium-5">
                            <div class="component text">
                                <p><?php echo $appliance->text; ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>
    </div>
</section>

<section id="appliances-text">
    <div class="grid-container">
        <div id="<?php echo str_replace('#','',getData()['appliances'][0]->href); ?>" class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12 medium-6">
                <div class="component text styled">
                    <h4><?php echo getData()['appliances'][0]->title; ?></h4>
                    <p>Platformica NAS appliance is a Platformica appliance cluster which provides NAS storage for file oriented access. The appliance uses CEPH as a backing block layer and Ganesha application server to provide NFS and CIFS access. There is also the Prometheus monitoring framework  with Grafana UI embedded into the appliance to provide monitoring and alerting. This appliance can be scaled up and down (adding servers or virtual machines into the appliance cluster) which scales up/down available space in filesystem exports. This appliance can be deployed on bare metal or virtual machines. When deciding the infrastructure platform, customers must consider what level of performance stability they require.</p>
                </div>
            </div>
            <div class="cell small-12 medium-6">
                <div class="component splashimage">
                    <div class="grid-x">
                        <div class="cell shrink">
                            <?php include("images/svg/nas.svg"); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="<?php echo str_replace('#','',getData()['appliances'][1]->href); ?>" class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12 medium-6 small-order-2 medium-order-1">
                <div class="component splashimage">
                    <div class="grid-x">
                        <div class="cell shrink">
                            <?php include("images/svg/appliances.svg"); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cell small-12 medium-6 small-order-1 medium-order-2">
                <div class="component text styled">
                    <h4><?php echo getData()['appliances'][1]->title; ?></h4>
                    <p>Platformica Load Balancer appliance is a Platformica appliance cluster which provides Elastic load balancing capabilities primarily for HTTP HTTPS traffic. It is based on Traefik and, as described in the architecture chapter the Traefik API is fully exposed and it is the API against which application or devops engineers integrate their applications. The appliance also embeds prometheus framework to provide monitoring and alerting for admins. This appliance also provides basic L3/L4 firewall capabilities in case the balancer is used in simpler deployments and exposed directly to Internet. The firewall is also available via API and is based on Linux nftables.</p>
                </div>
            </div>
        </div>

        <div id="<?php echo str_replace('#','',getData()['appliances'][2]->href); ?>" class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12 medium-6">
                <div class="component text styled">
                    <h4><?php echo getData()['appliances'][2]->title; ?></h4>
                    <p>Platformica router appliance is a Platformica appliance which provides L3 routing, firewalling and peering capabilities. The key software components used to build this appliances are FRR, Keepalived, nftables. FRR routing suite is a software suite to provide support for wide range of IP related protocols (BGP, IS-IS, LDP, OSPF, PIM, and RIP), nftables provide statefull L3/L4 firewalling capabilities and keepalived provides HA for this appliance. Again all configurations are available via API. On top of configurations of particular components (FRR, nftables), this appliance provides simplified high level API for easy configurations of most of the common use cases. For example there is a simple API which implements security groups like firewall configurations (simple IP and TCP stateless rules). This appliance can be deployed on bare matel or virtual machines. When using virtual machines it is recommended to use SR-IOV based virtual machines.</p>
                </div>
            </div>
            <div class="cell small-12 medium-6">
                <div class="component splashimage">
                    <div class="grid-x">
                        <div class="cell shrink">
                            <?php include("images/svg/router.svg"); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="<?php echo str_replace('#','',getData()['appliances'][3]->href); ?>" class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12 medium-6 small-order-2 medium-order-1">
                <div class="component splashimage">
                    <div class="grid-x">
                        <div class="cell shrink">
                            <?php include("images/svg/database.svg"); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cell small-12 medium-6 small-order-1 medium-order-2">
                <div class="component text styled">
                    <h4><?php echo getData()['appliances'][3]->title; ?></h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
            </div>
        </div>
    </div>
</section>



<?php get_footer();

