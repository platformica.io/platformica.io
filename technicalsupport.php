<?php

/**
 * Template Name: Platformica #technicalsupport
 *
 * @package platformica
 */

?>
<?php get_header(); ?>

<svg style="max-width:1500px" data-square='{ "image" : "https://www.platformica.io/wp-content/themes/platformica/images/png/d.png" , "threshold" : 0.1 , "pixel" : 2 , "gutter" : 2 , "width" : 1500 }' ></svg>
<section id="technicalsupport">
    <div class="grid-container">
        <div class="grid-x">
            <div class="cell small-12">
                <div class="component heading">
                    <h1>Technical support</h1>
                    <h4>Connect with us</h4>
                    <hr />
                </div>
            </div>
        </div>
        <div class="grid-x grid-margin-x">
            <div class="cell small-6">
                <div class="component block styled">
                    <?php include('images/svg/sample.svg'); ?>
                    <h4>Slack</h4>
                    <p>Professional Datacenter/Private cloud services</p>
                    <?php echo do_shortcode("[slack_signup]"); ?>
                </div>
            </div>
            <div class="cell small-6">
                <div class="component block styled">
                    <?php include('images/svg/sample.svg'); ?>
                    <h4>Customer portal</h4>
                    <p>Professional Datacenter/Private cloud services</p>
                    <a class="button" href="#">Go to support portal</a>
                </div>
            </div>
        </div>
    </div>
</section>


<?php get_footer();
