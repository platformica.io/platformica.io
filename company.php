<?php

/**
 * Template Name: Platformica #company
 *
 * @package platformica
 */

?>
<?php get_header(); ?>
<?php
    $customers_short = [];

    $customers_short[] = (object) [
        'src' => 'images/png/300x120/cra.png'
    ];
    $customers_short[] = (object) [
        'src' => 'images/png/300x120/ibuldings.png'
    ];
    $customers_short[] = (object) [
        'src' => 'images/png/300x120/pixelfederation.png'
    ];
    $customers_short[] = (object) [
        'src' => 'images/png/300x120/pygmalios.png'
    ];
    $customers_short[] = (object) [
        'src' => 'images/png/300x120/coresystems.png'
    ];
?>
<svg style="max-width:1500px" data-square='{ "image" : "https://www.platformica.io/wp-content/themes/platformica/images/png/d.png" , "threshold" : 0.1 , "pixel" : 2 , "gutter" : 2 , "width" : 1500 }' ></svg>
<section>
    <div class="grid-container">
        <div class="grid-x">
            <div class="cell small-12">
                <div class="component heading">
                    <h1>Company</h1>
                    <h4>Application infrastructure solutions for enterprises and operators</h4>
                    <hr />
                </div>
            </div>
        </div>
    </div>
</section>
<section id="whoweare" class="general">
    <div class="grid-container">
        <div class="grid-x">
            <div class="cell small-12">
                <div class="component heading">
                    <h1>Who we are?</h1>
                    <h4>Would you like to know some specifics?</h4>
                    <hr />
                </div>
            </div>
        </div>
        <div class="grid-x  grid-margin-x grid-margin-y">
            <div class="cell small-6">
                <div class="component styled">
                    <p>Platformica is European based infrastructure solutions and consulting company helping enterprises and operators build modern application environments.</p>
                    <p>We focus especially on transitioning to the new wave of technological advancements in the application infrastructure world. Specifically it is the adoption of public cloud services, DevOps automation, Opensource alternatives to traditional components and disaggregation.</p>
                </div>
            </div>
            <div class="cell small-6">
                 <div class="component styled">
                    <p>Our disaggregation approach stands on three key areas which together form complete end-to-end infrastructure strategy. The three key areas are - ability to build various infrastructures in their primary deployment schemes, ability to use various platform components (firewalls, load balancers, databases) and ability to automate heterogenous applications environments.</p>
                </div>
            </div>
        </div>
    </div>
</section>






<section id="ourvision">
    <div class="grid-container">
        <div class="grid-x">
            <div class="cell small-12">
                <div class="component heading">
                    <h1>Our vision</h1>
                    <h4>Would you like to know some specifics?</h4>
                    <hr />
                </div>
            </div>
        </div>
        <div class="grid-x  grid-margin-x grid-margin-y">
            <div class="cell small-12">
                <div class="component text styled">
                    <p>Platformica builds disaggregated infrastructure solutions which are technologically diversified according to customer’s workload portfolio. These solutions there provide optimal and economical application environment as particular technologiers are used in their most common scenarios, battle tested by milions other users and workloads avoiding the pitfalls of using special and niche features which only introduce strong vendor locks and extra costs. This approach then allows each part of the infrastructure to evolve at its own pace based on how respective ecosystem evolves.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="customers">
    <div class="grid-container">
        <div class="grid-x">
            <div class="cell small-12">
                <div class="component heading">
                    <h1>Customers</h1>
                    <h4>Would you like to work with us?</h4>
                    <hr />
                </div>
            </div>
        </div>
        <div class="grid-x">
            <div class="cell small-12">
                <div class="component cloud">
                    <!-- ??? //-->
                    <ul class="grid-x">
                        <?php foreach($customers_short as $customer): ?>
                        <li class="cell small-3">
                            <div>
                                <img src="<?php echo get_template_directory_uri(); ?>/<?php echo $customer->src; ?>" />
                            </div>
                        </li>
                        <?php endforeach; ?>
                    </ul>
                    <!-- ??? //-->
                </div>
            </div>
        </div>
        <div class="grid-x">
            <div class="cell small-12">
                <div class="component">
                    <a class="button" href="#">Learn more</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="careers">
    <div class="grid-container">
        <div class="grid-x">
            <div class="cell small-12">
                <div class="component heading">
                    <h1>Careers</h1>
                    <h4>Would you like to know some specifics?</h4>
                    <hr />
                </div>
            </div>
        </div>
        <div class="grid-x">
            <div class="cell medium-offset-1 small-10">
                <div class="component styled">
                    <p>We are looking for new colleagues into our two teams in Prague and Bratislava. We are mainly looking for engineers interested in building, operating or automating IT infrastructures and for developers interested in open source software development in areas of systems and networks (not really a kernel hacking, but all the layers above kernel). Send us an email if you are interested in following technologies, services, paradigms and tools:</p>
                </div>
            </div>
        </div>
        <div class="grid-x">
            <div class="cell medium-offset-3 small-6">
                <div class="component cloud" >
                    <?php include('images/svg/cloud.svg'); ?>
                </div>
            </div>
        </div>
        <div class="grid-x">
            <div class="cell medium-offset-1 small-10">
                <div class="component styled">
                    <p>We are a friendly team and we offer flexible working environment (remote work /home office possible). Both juniors and seasoned experts are welcomed and we look forward hering from you!</p>
                    <p>Contact us at <a href="mailto:work@platformica.io">work@platformica.io</a></p>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer();
