<?php

/**
 * Template Name: Platformica #solutions1
 *
 * @package platformica
 */

?>
<?php get_header(); ?>
<?php
    $solutions = [getData()['solutions'][0],getData()['solutions'][1],getData()['solutions'][2]];



    $solutions[2]->title = 'Infrastructure for Containers';
    $solutions[1]->title = 'Infrastructure for Cloud-native';
    $solutions[0]->title = 'Networking/NFV components';
    $solutions[0]->href = '/appliances/';


?>
<svg style="max-width:1500px" data-square='{ "image" : "https://www.platformica.io/wp-content/themes/platformica/images/png/d.png" , "threshold" : 0.1 , "pixel" : 2 , "gutter" : 2 , "width" : 1500 }' ></svg>
<section id="solution-1">
    <div class="grid-container">
        <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12">
                <div class="component heading">
                    <h1>Simplistic VMware</h1>
                    <h4>Application infrastructure solutions for enterprises and operators</h4>
                    <hr />
                </div>
            </div>
        </div>
    </div>
</section>
<section id="solutions-1-1" class="general">
    <div class="grid-container">
        <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12 medium-6 small-order-2 medium-order-1">
                <div class="component text styled">
                    <h4>Optimizing VMware costs</h4>
                    <p>VMware is the best infrastructure platform for legacy workload and offers many crucial features such as live migration or virtual machine high availability to provide highly reliable infrastructure for traditional IT applications.  Introduction of cloud-native application models and containerization however moved reliability from infrastructure layer into platform/application layer and modern cloud-native or containerized applications do not require very reliable underlying infrastructure. This allows building more economical infrastructures for these types of applications which do not have high-end VMware features and thus are not burdened with expensive VMware licensing.</p>
                </div>
            </div>
            <div class="cell small-12 medium-6 small-order-1 medium-order-2">
                <div class="component splashimage">
                    <div class="grid-x">
                        <div class="cell shrink">
                            <?php include("images/svg/vmware.svg"); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="solutions-1-2" class="forceblack">
    <div class="grid-container">
        <div class="grid-x grid-margin-x grid-margin-y">
            <div class="cell small-12">
                <div class="component text styled">
                    <h4>Reducing vendor lock-in</h4>
                    <p>We provide VMware solutions which are suited for customers who want to use VMware for their traditional applications but want to keep cloud-native and containerized workload outside of VMware ecosystem. All this to have better, full featured and more standard experience for respective application environment (Traditional, cloud-native, containerized) and to optimize VMware costs. Via infrastructure diversification we help our customers to keep VMware vendor lock in reasonable limits to maintain sustainable long term infrastructure strategy and to allow each part of infrastructure to evolve at its own pace.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="grid-container">
        <div class="grid-x" style="align-items: center;">
            <div class="cell small-12 medium-7">
                <div data-avoid  class="component text styled">
                    <h4 >Supporting diversified workloads</h4>
                    <p >We help customers to optimize existing VMware infrastructure or build a completely new one. We design and build minimalist style VMware infrastructures which are easy to integrate with the rest of the datacenter.</p>
                    <p style="max-width: 650px;">We design our VMware solutions specifically with disaggregation of following three key areas from the VMware ecosystem in mind.</p>
                </div>
                <div class="component bullets">
                    <br class="hide-for-small-only" /><br class="hide-for-small-only" />
                    <div class="grid-x">
                        <div class="cell shrink">
                            <div data-avoid class="pad"><svg data-track='{ "link" : "002", "offset" : [[0,0],[18,135],[40,45],[250,0]] }' data-pad="{}" /><a style="color:inherit; text-decoration: none;font-size: 14px;" href="<?php echo $solutions[0]->href; ?>" ><?php echo $solutions[0]->title; ?></a></div>
                        </div>
                        <div class="cell shrink">
                            <div data-avoid class="pad"><svg data-track='{ "link" : "001", "offset" : [[0,0],[18,235],[40,315],[250,0]] }' data-pad="{}" /><a style="color:inherit; text-decoration: none;font-size: 14px;" href="<?php echo $solutions[2]->href; ?>" ><?php echo $solutions[2]->title; ?></a></div>
                        </div>
                        <div class="cell shrink">
                            <div data-avoid class="pad"><svg data-track='{ "link" : "003", "offset" : [[0,0],[18,235],[40,315],[250,0]] }' data-pad="{}" /><a style="color:inherit; text-decoration: none;font-size: 14px;" href="<?php echo $solutions[1]->href; ?>" ><?php echo $solutions[1]->title; ?></a></div>
                        </div>
                    </div>
                    <br class="hide-for-small-only" /><br class="hide-for-small-only" />
                </div>
                <div data-avoid class="component text styled">
                    <p >Customers that have more complex, heterogeneous datacenter networking can also disaggregate VXLAN networking from VMware-centric SDN (NSX) and instead leverage modern VXLAN-based EVPN DC fabrics</p>
                </div>
            </div>
            <div class="cell small-12 medium-5 hide-for-small-only">
                <h4>&nbsp</h4><h4>&nbsp</h4>
                <div class="component nextsolution">
                    <div class="grid-x">
                        <div class="cell small-12 medium-12 large-6">
                            <div class="grid-x" style="justify-content: flex-end;">
                                <div  class="cell small-12" style="max-width: 250px;">
                                    <div  data-avoid class="overlay" style="background: url('<?php echo get_template_directory_uri(); ?>/<?php echo $solutions[0]->src; ?>') center center/cover;"><svg data-track='{ "link" : "002", "offset" : [[0,0],[48,180]]  }' data-pad='{ "width" : "45", "height" : "45" }' /></div>
                                </div>
                                <div  class="cell small-12" style="max-width: 250px;">
                                    <div  data-avoid  class="overlay" style="background: url('<?php echo get_template_directory_uri(); ?>/<?php echo $solutions[1]->src; ?>') center center/cover;"><svg data-track='{ "link" : "003", "offset" : [[0,0],[48,180]] }' data-pad='{ "width" : "45", "height" : "45" }' /></div>
                                </div>
                            </div>
                        </div>
                        <div class="cell small-12 medium-12 large-6">
                            <div class="grid-x" style="justify-content: flex-end;">
                                <div  class="cell small-12" style="max-width: 250px;">
                                    <div data-avoid  class="overlay" style="background: url('<?php echo get_template_directory_uri(); ?>/<?php echo $solutions[2]->src; ?>') center center/cover;"><svg data-track='{ "link" : "001", "offset" : [[0,0],[48,180]]  }' data-pad='{ "width" : "45", "height" : "45" }' /></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>




<?php get_footer();
